// reference generator
// babyjeans

const fs = require('fs-extra');
const readline = require('readline');

const scripts = [
    "imguigml_functions",
    "imguigml_wrapper",
    "imguigml_wrapper_widget"
]

let reference = [ ];

async function generate_ref(script) { 
    let filePath = "./yyp/scripts/" + script + "/" + script + ".gml"; 
    const fileStream = fs.createReadStream(filePath);
    const rl = readline.createInterface({
        input: fileStream,
        crlfDelay: Infinity
    });

    let func = undefined;

    for await (let line of rl) {
        line = line.trim();

        if (!line.startsWith('///')) {
            func = undefined;
            continue;
        }

        line = line.replace("///", "");
        line.trim();

        if (line.startsWith("@function")) {
            let funcCall = line.replace("@function", "").trim();
            let paramIndex = funcCall.indexOf("(");
            let name = funcCall;
            if (paramIndex != -1) {
                name = funcCall.substr(0, paramIndex);
            }

            if (funcCall.indexOf("(") == -1) {
                funcCall += "()";
            }

            func = { 
                name: name,
                call: funcCall,
                desc: "",
                params: [ ],
                retval: { }
            };

            reference.push(func);
        } else if (func != undefined) {
            if (line.startsWith("@param") || line.startsWith("@arg")) {
                let paramLine = line.replace("@param", "").trim();
                paramLine = paramLine.replace("@arg", "").trim();

                let param = { 
                    type: "",
                    desc: ""
                };
                
                let paramTypeBegin = paramLine.indexOf("{");
                if (paramTypeBegin != -1) {
                    let paramTypeEnd = paramLine.indexOf("}");
                    param.type = paramLine.slice(0, paramTypeEnd + 1);
                    paramLine = paramLine.slice(paramTypeEnd + 1).trim();
                } 
                
                let paramNameEnd = paramLine.indexOf(' ');
                if (paramNameEnd == -1) {
                    paramNameEnd = paramLine.indexOf('  ');
                }

                if (paramNameEnd == -1) {
                    param.name = paramLine.trim();
                } else {
                    param.name = paramLine.slice(0, paramNameEnd + 1).trim();
                    paramLine = paramLine.slice(paramNameEnd + 1).trim();
                    
                    if (paramLine.length != 0) {
                        param.desc = paramLine;
                    }
                }
                
                func.params.push(param);
            } else if (line.startsWith("@desc")) {
                let description = line.replace("@description", "").trim();
                description = description.replace("@desc", "").trim();
                func.desc += description;
            } else if (line.startsWith("@return")) {
                let ret = line.replace("@returns", "").trim();
                ret = ret.replace("@return", "").trim();

                let type = undefined;
                let typeBegin = ret.indexOf('{');
                if (typeBegin != -1) {
                    let typeEnd = ret.indexOf('}');
                    type = ret.substr(0, typeEnd + 1);
                    ret = ret.slice(typeEnd + 1, ret.length - typeEnd).trim();
                    func.retval.type = type;
                }

                let desc = ret.trim();
                if (desc.length > 0) {
                    func.retval.desc = desc;
                }
                
            } else {
                line = line.replace("@", "").trim();
                func.desc += "\n" + line;
            }
        }
    }
}

function convert_markdown(func) {
    let markdown = "";
    markdown += "#### `" + func.name + "`\n";
    if (func.params.length > 0) {
        markdown += "| | | \n";
        let maxLenName = 0;
        let maxLenDesc = 0;

        for (const param of func.params) {
            maxLenName = Math.max(maxLenName, param.name.length);
            maxLenDesc = Math.max(maxLenDesc, param.type.length + param.desc.length + 1);
        }

        markdown += "| ";
        maxLenName += 4;
        const nameDash = maxLenName - 2;
        for (let i = 0; i < nameDash; ++i) {
            markdown += '-';
        }

        markdown += ' | ';
        for (let i = 0; i < maxLenDesc; ++i) {
            markdown += '-';
        }
        markdown += ' |\n';
        for (const param of func.params) {
            markdown += '**' + param.name + '**';
            for (let i = 0; i < nameDash - param.name.length - 2; ++i) {
                markdown += ' ';
            }

            markdown += ' | ';
            markdown += param.type + ' ' + param.desc + '\n';
        }
    }
  
    markdown += "**returns**"; 
    if (func.params.length != 0) {
        markdown += " | ";
    } else {
        markdown += ": ";
    }

    if (func.retval.type != undefined || func.retval.desc != undefined) {
        if (func.retval.type != undefined) {
            markdown += func.retval.type + ' ';
        }

        if (func.retval.desc != undefined) {
            markdown += func.retval.desc;
        }
    } else {
        markdown += "None";
    }
    markdown += '\n';
    markdown += '```\n' + func.call + '\n```\n'
    
    if (func.desc.length > 0) {
        markdown += func.desc + '\n';
    }

    markdown += '\n---\n\n';

    return markdown;
}

(async function() { 
    for (let script of scripts) {
        await generate_ref(script);
    }

    let markdown = "## Reference\n\n";
    for (let func of reference) {
        markdown += convert_markdown(func);
    }

    fs.writeFileSync("./submission/README_ref.md", markdown);
})();