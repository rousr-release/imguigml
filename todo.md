*   Add support for new OpenPopup*(), BeginPopupContext*(), IsPopupOpen() functions.
*   Add support for Table functions
*   Add support for InvisibleButton flags
*   Update `AddPolyline`, `PathStroke`, 
*   Wrap `AddImageRounded`
*   Double Check `EImGui_TextCallbackData`
*   Double Check `EImGui_MouseCursor` usage in imgui End Step
*   Consider `ImGuiNavInput_`, `ImGuiSortDirection_`, `ImGuiDir_`, and `ImGuiConfigFlags_`