// buildCookbook.js
//  babyjeans
///
let scryyptPath = "../scryypt/";

const fs               = require("fs-extra");
const child_process    = require('child_process');
const MarkDownIt       = require('markdown-it');
const MarkDownItAnchor = require('./submission/scripts/markdown-it-cookbook.js');
const YYP              = require(scryyptPath + "script/yyp/yyp.js");

console.log("Reading cookbook.md")
let cookbook = fs.readFileSync("cookbook.md").toString();

console.log("Rendering cookbook.md -> html");
let md = MarkDownIt({html:true}).use(MarkDownItAnchor, { });
let indexhtml = md.render(cookbook);

console.log("Writing cookbook/index.html");
fs.writeFileSync("index.html", indexhtml);

console.log("Now execute:\n" + 
"   `scp index.html grogshack:/home/webhost/imguigml.rou.sr/cookbook.html`");