// buildReadMe.js
//  babyjeans
///

const fs = require("fs-extra");
const child_process = require('child_process');
const MarkDownIt = require('markdown-it');
const MarkDownItAnchor = require('./submission/scripts/markdown-it-anchor.js');

let DoUploadReadMe = true;

console.log("Building README")

let readme_banner = fs.readFileSync("submission/README_banner.md");
let readme_dev    = fs.readFileSync("submission/README_dev.md");
let readme_intro  = fs.readFileSync("submission/README_intro.md");
let readme_doc    = fs.readFileSync("submission/README_doc.md");
let readme_ref    = fs.readFileSync("submission/README_ref.md");
let readme_changelist = fs.readFileSync("submission/README_changelist.md");

console.log("  - writing README.md");
fs.writeFileSync("README.md", readme_banner + readme_dev + readme_intro + readme_doc + readme_ref + readme_changelist);

console.log("  - writing imguigml_read_me.gml");
let readmegml = "/*\n" + readme_intro + readme_doc + readme_ref + readme_changelist + "\n*/";
fs.writeFileSync("yyp/scripts/imguigml_read_me/imguigml_read_me.gml", readmegml);

if (DoUploadReadMe) {
    console.log("Uploading to http://imguigml.rou.sr/")
    let md = MarkDownIt({html:true}).use(MarkDownItAnchor, { });

    let indexhtml = md.render(readme_banner + readme_intro + readme_doc + readme_ref + readme_changelist);
    fs.writeFileSync("index.html", indexhtml);

    child_process.execSync("scp index.html grogshack:/home/webhost/imguigml.rou.sr/index.html");
    child_process.execSync("rm index.html");

    console.log("   - Uploaded.");
}

console.log("Complete")