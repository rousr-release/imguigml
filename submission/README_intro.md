## Overview

**ImGuiGML** is a DLL / GML wrapper of the popular [Dear ImGui,](https://github.com/ocornut/imgui) library.
  
It's available on the **Windows**, **macOS**, and **Ubuntu** platforms:
* Windows support includes _x86_ and _x64_
* macOS support includes _Intel_ and _M1_ architectures
* Ubuntu support includes _x64_ support

ImGui is a UI library designed for fast implementation and iteration, using an 'Immediate Mode GUI' paradigm You will primarily be putting your GUI functions in step events that are called each step, rather than an event / retained based paradigm of only making updates.

_Extension by babyjeans (Follow [@babyj3ans](https://twitter.com/babyj3ans/))_   
_Special Thanks to [@net8floz](https://twitter.com/net8floz/) for additional programming and the excellent example screenshots!_

_**Special** Special Thanks to [JujuAdams](https://twitter.com/jujuadams) for supporting the project!_
