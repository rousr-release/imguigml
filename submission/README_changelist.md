### Changelist

* **v1.100.4**
  * Fix `imkeys` array to skip over the new ImGui Keypad Enter addition (fixes a bunch of keys in the input text boxes)
  * Fix a whole bunch of ImGui enums, and make sure they're all up to date.
  * Rename `sr_buffer` functions as to not break compatibility with rousrCore.
* **v1.100.3**
  * Fix `imguigml_begin_drop_target`
  * Fix for Windows / Linux DLLs
* **v1.100.1**
  * Add Linux support
  * Fixed `imguigml_add_font_for_ttf`
* **v1.100.0**
  * Fixed `buffer_set_surface` call
  * Update to ImGui v1.83.0
    *   `imguigml_set_scroll_here` is now split into `imguigml_set_scroll_here_x` and `imguigml_set_scroll_here_y`
    *   Removed `imguigml_is_any_window_hovered`
    *   Removed `imguigml_calc_item_rect_closest_point`
    *   Updated all `drag` and `slider` widgets so that `power` was removed, now using `EImGui_SliderFlags`
  * Readded macOS build - arm64 + x86_64 support
  * Added x64 support
  * Updated for GMS 2.3+
* **v1.90.0**
  * Update to ImGui v1.71.0
    * Changes some API
  * Minor fixes
  * **NOTE: NO MAC BUILD AVAILABLE**
* **v1.82.0**
  * Added macOS version
* **v1.81.0**
  * Fixed Sprite Atlas growing - support for larger than 2048x2048 atlases.
* **v1.80.3**
  * Fix Radio Button return value
  * Add ImGui Demo button.
* **v1.80.2** 
  * Updated `extRousrDs` 
  * Added more options to `imguigml_config` for overriding the cursor and reporting errors without aborting.
* **v1.80.1**
  * Fixed up the `required` / `not required`
* **v1.80.0**
  * Adds `show_error` calls when ImGui asserts due to a misuse of imgui functions.
  
* **v1.75.1** 
  Fixed drawlist sprite drawing to work like the other sprite imguigml items. 
  * Fixed `imguigml_drawlist_add_sprite`, removed uv params
  * Removed `imguigml_drawlist_add_sprite_quad`
* **v1.75.0** - Soft update to v1.53 - no new functions wrapped, but api updated.
  * **Fixes**
    * `imguigml_sprite` and `imguigml_sprite_button` now draw trimmed sprites and sprite_added sprites correctly. (added sprite cache) 
  * **Breaking Changes**
    * `imguigml_get_items_line_height_with_spacing` is now `imguigml_get_items_line_height_with_spacing`
    * removed `imguigml_is_root_window_focused()`  **see focused flags**
    * removed `imguigml_is_root_window_or_any_child_focused()` **see focused flags**
    * removed `imguigml_is_root_window_or_any_child_hovered()` **see focused flags**
    * removed `imguigml_set_next_window_content_width()`
    * removed `EImGuiGML_WindowFlags`: `ShowBorders`
  * **API Changes**
    * `imguigml_drawlist_add_poly` and `imguigml_drawlist_add_convex_poly_filled` no longer accept AA param
    * `imguigml_is_window_focused` now takes `EImGui_FocuseFlags`
  * **ADDED** (from 1.53)
    * `imguigml_drawlist_set_flags` and `imguigml_drawlist_get_flags` for messing with antialiasing (replacing the param above)
    * `imguigml_drawlist_use_overlay` and `imguigml_drawlist_use_window` allowing use of the new overlay
    * `imguigml_style_colors_light`
    * `imguigml_io_set_cursor_blink` and get.
    * `imguigml_show_demo_window`, `imguigml_show_metrics_window`, `imguigml_show_style_selector`, `imguigml_show_user_guide`, `imguigml_show_font_selector`
    * `imguigml_begin_drag_drop_source`, `imguigml_set_drag_drop_payload`, `imguigml_end_drag_drop_source`, `imguigml_begin_drop_target`, `imguigml_accept_drag_drop_payload`, `imguigml_end_drag_drop_target`
    * `imguigml_payload`, `imguigml_payload_get`
    * Flags
      * Added `PopupRounding`, `WindowBorderSize`, `FrameBorderSize`, `ChildBorderSize` to `EImGui_StyleVar`
      * Added `EImGui_InputTextFlags.NoUndoRedo`
      * Added `EImGui_WindowFlags`: `ResizeFromAnySide`
      * Added `EImGui_FocusedFlags` to pass to `imguigml_is_window_focused`, replacing the obsolete root_window functions
      * Added `EImGui_HoveredFlags` `RootWindow` and `ChildWindow` to include in hovered tests
      * Added `EImGui_DrawCornerFlags` for rounding flags
    
* **v1.3.0** - BIG update, here's a list of some of the fixes:
  * **v1.3.1** Hot Fix: Fixed a few scaling display issues, fixed texteditor buffer size limitations
  * **v1.3.2** Hot Fix: Add sprite caching
  *  Features
     *  Don't draw if a surface/texture doesn't exist (avoids white boxes flashing)
     *  Add ability to draw at depth
     *  Can draw in `Draw` event
  *  API 
     *  Added float4 -> gml color conversion functions 
  *  Fixes:
     *  `imguigml_texteditor_get_text` returned nothing, also fixed indexing text editors
     *  Fixed ImGuiGML bashing the cursor so that resize cursors, etc, didn't function properly.
     *  **Fixed ImGuiGML Display Issues when resizing the window or GUI Layer... more to come on this**
  *  Various doc fixes!
  
* **v1.2.3**
  * API
    * Added `imguigml_surface` and refactored how sprite and 'image' works. `imguigml_image` still takes a texture pointer, but there's rarely a reason to use it.
  * Fixes:
    * Fixed `draw_add_rect_filled`
    * Automatically multiply the normalized pixel percent trimmed from sprites when useing `imguigml_sprite`
    * `imguigml_button` now accepts 1 param for size
    * Fix `imguigml_color_convert_` gml / u32 functions being backwards
     
* **v1.1.3: Extras!**
  * Features
      * Added: ImDrawList, ImGui_Tabs, and ImGuiColorTextEdit
  * API: 
    * Added `imguigml_color_convert_u32_to_gml` and `imguigml_color_convert_gml_to_u32`
    * Added `imguigml_texteditor_*` functions (38 functions)
    * Added `imguigml_begin_tab_bar` `imguigml_tab_item` `imguigml_set_tab_item_closed`, +4 more tab functions
    * **API CHANGE:** 
      * Fixed `imguigml_checkbox` to now return an array: `[_changed, _val]` where _val is the current true/false of the checbbox. Before it only returned `_changed`
      * Fixed `imguigml_checkbox_flags` to now return an array: `[_changed, _val]` where _val is the current true/false of the checbbox. Before it only returned `_changed`
  * Fixes:
    * `imguigml_checkbox` and `imguigml_checkbox_flags` fixes from above
    * `imguigml_imguigml_color_convert_float4_to_u32` interanlly, param 3/4 were swapped.
    * `imguigml_tree_node` was accidentally left unimplemented
    * Various help / param issues fixed in the completion.
    * Slightly improved the demo.

* **v1.0.0: Public Release!!!**
  * Added: `imguigmlg_toggle_cursor` for preventing ImGui from touching your cursor, also added config lines for it.
  * Fixed: Crash when switching rooms while imgui is active, various function documentation was incorrect.

* **v0.9.0: RC1**
  * Added: io settings, Callbacks for InputText and InputTextMultiLine (ImGuiTextEditCallback), **Reference docs, thanks @net8floz**
  * Fixed: `input_int` crashing and using a float value 

* **v0.5.0:** Last Preview 'Major' before RC1
  *  Fixed: Clean Up event crashes and memory leaks, YYC crash in `imguigml_set_next_window_pos`
  *  Added: `imguigml_activate` `imguigml_deactivate` and `imguigml_ready` helpers

* **v0.2.0:** First "private" release
  * **v0.2.1:** Hot Fix: Fixed: `imguigml_plot_histogram` _overlay param
  * **v0.2.3:** Hot Fix: BeginChild/EndChild weren't implemented, removed IM_ASSERT from the DLL
  * **v0.2.4:** HotFix 
    *  Fixed: `set_column_width` call and input field returns
    *  Added: **MouseWheel** Support, Added **Keyboard Input** Support
    *  Optimized Render thread ever so slightly.
  * **v0.2.5:** HotFix - Fixed: `plot_lines` arguments, "help" parser
  * **v0.2.6:** Added: `imguigml_sprite` and `imguigml_sprite_button` support

#### Unsupported functions

**Probably never coming**

*   **IO functions:** Not sure what you're going to try to pull here..
    *   Memory Manager
        *   ImGui::MemAlloc
        *   ImGui::MemFree

### Credits

ImGuiGML DLL was written by [@babyj3ans](https://twitter.com/babj3ans/)
with tons of help and example work by [@net8floz](https://twitter.com/net8floz/)

Created with:

* [ImGui](https://github.com/ocornut/imgui) - Of course
* [ImGuiColorTextEdit](https://github.com/BalazsJako/ImGuiColorTextEdit) - TextEditor implementation