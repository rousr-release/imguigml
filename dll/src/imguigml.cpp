#include <vector>
#include <sstream>

#include <cstdlib>

#include "imguigml.h"

#include "imgui/imgui.h"
#include "iggrenderbuffer.h"

#include "rousrGML/GMLBuffer.h"
#include "rousrGML/GMLTypedBuffer.h"

#include "imgui wrappers/imguigml_wrappers.h"

#if defined(_WIN32)
#include <windows.h>
#else
#include <sys/stat.h>
#endif

namespace {
    
    std::unique_ptr<ImGuiGML> imguigml;

    bool file_exists(std::string fname) {
#if defined(_WIN32)
        std::wstring wfname = widen(fname);
        DWORD dwAttrib = GetFileAttributesW(wfname.c_str());
        return (dwAttrib != INVALID_FILE_ATTRIBUTES &&
            !(dwAttrib & FILE_ATTRIBUTE_DIRECTORY));
#else
        struct stat sb;
        return (!stat(fname.c_str(), &sb) && S_ISREG(sb.st_mode));
#endif
    }

    std::vector<std::string> string_split(std::string str, char delimiter) {
        std::vector<std::string> vec;
        std::stringstream sstr(str);
        std::string tmp;
        while (std::getline(sstr, tmp, delimiter)) {
            vec.push_back(tmp);
        }
        return vec;
    }

}

const std::unique_ptr<ImGuiGML>& ImGuiGML::Instance() { return imguigml; }
std::shared_ptr<CGMLBuffer> ImGuiGML::mDebugBuffer = nullptr;

void ImGuiGML::AssertFailed(bool cond, const char* _msg, const char *_file, int _line) {
    if (!cond) {
        if (mDebugBuffer != nullptr) {
            auto pos(mDebugBuffer->Tell());
            mDebugBuffer->Seek(0);
            auto count(mDebugBuffer->Read<uint32_t>());
            mDebugBuffer->Seek(0);
            mDebugBuffer->Write<uint32_t>(count + 1);
            mDebugBuffer->Seek(pos);
            mDebugBuffer->Write<std::string>("Assertion failed: " + std::string(_msg) + " (" + std::string(_file) + " Line: " + std::to_string(_line) + ")");
        }
    }
}

void ImGuiGML::FlushDebugBuffer() {
    if (mDebugBuffer != nullptr) {
        mDebugBuffer->Seek(0);
        mDebugBuffer->Write<uint32_t>(0);
    }
}

ImGuiGML::ImGuiGML() {
    mContext = ImGui::CreateContext();
    ImGui::SetCurrentContext(mContext);
    auto& io(ImGui::GetIO());

    std::vector<std::string> fonts;
    const char *envFontFiles = environment_get_variable("IMGUI_FONT_FILES");
    fonts = string_split((strcmp(envFontFiles, "") != 0) ? envFontFiles : imguigml_get_fontfiles(), '\n');

    ImFontConfig config;
    io.IniFilename = nullptr;
    config.MergeMode = true;

    ImFont* font = nullptr;
    ImWchar ranges[] = { 0x0020, 0xFFFF, 0 };
    const char *envFontSize = environment_get_variable("IMGUI_FONT_SIZE");
    float fontsize = (float)((strcmp(envFontSize, "") != 0) ? strtod(envFontSize, nullptr) : imguigml_get_fontsize());

    io.Fonts->ClearFonts();
    for (unsigned i = 0; i < fonts.size(); i++) {
        if (file_exists(fonts[i])) {
            io.Fonts->AddFontFromFileTTF(fonts[i].c_str(), fontsize, (!i) ? nullptr : &config, ranges);
        }
    }

    if (!io.Fonts->Fonts.empty())
        io.Fonts->Build();
    mShuttingDown = false;
    for (uint32_t i(0); i < ImGuiKey_COUNT; ++i)
        io.KeyMap[i] = i;   
}

ImGuiGML::~ImGuiGML() {
    mShuttingDown = true;
    if (mContext != nullptr)
        ImGui::DestroyContext(mContext);
}

void ImGuiGML::SetRenderBuffer(char *_cmdBuffer, uint32_t _cmdBufferSize, char *_vertexBuffer, uint32_t _vertexBufferSize) {
    mRenderBuffer = std::make_shared<IGGRenderBuffer>(_cmdBuffer, _cmdBufferSize, _vertexBuffer, _vertexBufferSize);
}

void ImGuiGML::SetWrapperBuffer(char *_wrapperBuffer, uint32_t _wrapperBufferSize) {
    mWrapperBuffer = std::make_shared<CGMLBuffer>(static_cast<size_t>(_wrapperBufferSize), _wrapperBuffer);
}

void ImGuiGML::SetDebugBuffer(char *_buffer, uint32_t _size) {
    mDebugBuffer = std::make_shared<CGMLBuffer>(static_cast<size_t>(_size), _buffer);
    mDebugBuffer->Seek(0);
    mDebugBuffer->Write<uint32_t>(0);
}

CGMLBuffer& ImGuiGML::WrapperBuffer() const { 
    if (mWrapperBuffer) 
        mWrapperBuffer->Seek(0); 
    return *mWrapperBuffer; 
}

void ImGuiGML::CreateFontTexture(uint32_t _texId) {
    auto& io(ImGui::GetIO());
    io.Fonts->GetTexDataAsRGBA32(&mFontBytes, &mFontWidth, &mFontHeight);
    io.Fonts->SetTexID(reinterpret_cast<void*>(static_cast<size_t>(_texId)));
}

void ImGuiGML::WriteFont(char *_fontBuffer) {
    std::memcpy(_fontBuffer, mFontBytes, mFontWidth * mFontHeight * 4u);
}

void ImGuiGML::UpdateOutput(CGMLTypedBuffer& _buffer) {
    auto& io(ImGui::GetIO());

    _buffer.Write<int8_t>(io.WantCaptureMouse);
    _buffer.Write<int8_t>(io.WantCaptureKeyboard);
    _buffer.Write<int8_t>(io.WantTextInput);
    _buffer.Write<int8_t>(io.WantSetMousePos);

    _buffer.Write<float>(io.Framerate);

    // TODO: Support more metricc
    _buffer.Write<int32_t>(io.MetricsActiveAllocations);
    _buffer.Write<int32_t>(io.MetricsRenderVertices);
    _buffer.Write<int32_t>(io.MetricsRenderIndices);
    _buffer.Write<int32_t>(io.MetricsActiveWindows);

    if (io.MouseDelta.x == -FLT_MAX) {
        _buffer.Write<int8_t>(0);
        _buffer.Write<int8_t>(0);
    } else {
        _buffer.Write<float>(io.MouseDelta.x);
        _buffer.Write<float>(io.MouseDelta.y);
    }
}

void ImGuiGML::UpdateInput(CGMLBuffer& _inputBuffer) {
    auto& io(ImGui::GetIO());
    
    float mouseX(_inputBuffer.Read<float>());
    float mouseY(_inputBuffer.Read<float>());
    io.MousePos = ImVec2(mouseX, mouseY);

    const uint32_t GMLMouseButtonCount(3);
    const uint32_t ImGuiMouseButtonCount(5);
    
    for (uint32_t mouseIndex(0); mouseIndex < ImGuiMouseButtonCount; ++mouseIndex)
        io.MouseDown[mouseIndex] = (mouseIndex < GMLMouseButtonCount ? _inputBuffer.Read<uint8_t>() == 1 : false);

    for (uint32_t keyIndex(0); keyIndex < ImGuiKey_COUNT; ++keyIndex)
        io.KeysDown[keyIndex] = _inputBuffer.Read<uint8_t>() == 1;
    
    io.KeyCtrl  = _inputBuffer.Read<uint8_t>() == 1;
    io.KeyAlt   = _inputBuffer.Read<uint8_t>() == 1;
    io.KeyShift = _inputBuffer.Read<uint8_t>() == 1;
    //io.KeySuper = _inputBuffer.Read<uint8_t>() == 1;
    io.KeySuper = false; // todo: add KeySuper

    io.MouseWheel = mMouseWheelSpeed * static_cast<float>(_inputBuffer.Read<int8_t>());
    io.MouseDrawCursor = _inputBuffer.Read<uint8_t>() == 1;
}

void ImGuiGML::BeginStep(double _deltaTime) {
    mRenderLock.lock();

    auto& io(ImGui::GetIO());
    io.DeltaTime = static_cast<float>(_deltaTime);
    io.DisplaySize.x = mDisplayWidth;
    io.DisplaySize.y = mDisplayHeight;


    ImGui::NewFrame();
}
namespace {
    std::atomic<bool> dataReady(false);
}
void ImGuiGML::EndStep() {
    ImGui::Render();

    auto drawData(ImGui::GetDrawData());
    dataReady = true;

    if (!mThreadStarted) {
        // write it to a GML buffer
        std::thread([this, drawData]() {
            mThreadStarted = true;
            while (!mShuttingDown) {
                while (!dataReady && !mShuttingDown)
                    std::this_thread::yield();

                if (mShuttingDown)
                    break;

                mRenderBuffer->WriteBuffer(drawData);
                dataReady = false;
                mRenderLock.unlock();
            }
            
            mRenderLock.unlock();
            mThreadStarted = false;
        }).detach();
    }
}

void ImGuiGML::WaitForBuffer() {
    mRenderLock.lock();
    mRenderLock.unlock();
}

#define CheckInit() { if (imguigml == nullptr) return -1.0; }

extern "C" {
    DLL_API double Init() {
        if (imguigml != nullptr)
            return -1.0;

        imguigml = std::make_unique<ImGuiGML>();
        return 1.0;
    }

    DLL_API double Shutdown() {
        CheckInit(); 
        imguigml.reset();
        return 1.0;
    }

    DLL_API double SetRenderBuffer(char *_cmdBuffer, double _cmdBufferSize, char *_vertexBuffer, double _vertexBufferSize) {
        CheckInit();
        imguigml->SetRenderBuffer(_cmdBuffer, static_cast<uint32_t>(_cmdBufferSize), _vertexBuffer, static_cast<uint32_t>(_vertexBufferSize));
        return 1.0;
    }

    DLL_API double SetWrapperBuffer(char *_wrapperBuffer, double _wrapperBufferSize) {
        CheckInit();
        imguigml->SetWrapperBuffer(_wrapperBuffer, static_cast<uint32_t>(_wrapperBufferSize));
        return 1.0;
    }

    DLL_API double SetDebugBuffer(char *_buffer, double _bufferSize) {
        ImGuiGML::SetDebugBuffer(_buffer, static_cast<uint32_t>(_bufferSize));
        return 1.0;
    }

    DLL_API double FlushDebugBuffer() {
        ImGuiGML::FlushDebugBuffer();
        return 1.0;
    }

    DLL_API double SetDisplaySize(double _width, double _height) {
        CheckInit();
        imguigml->SetDisplaySize(static_cast<float>(_width), static_cast<float>(_height));
        return 1.0;
    }

    DLL_API double GetDisplaySize() {
        CheckInit();

        float width(0.0f), height(0.0f);
        imguigml->GetDisplaySize(width, height);

        CGMLBuffer& out(imguigml->WrapperBuffer());
        out.Write<float>(width);
        out.Write<float>(height);
        return 1.0;
    }

    DLL_API double CreateFontTexture(double _textureId, char *_argBuffer) {
        CheckInit();
        imguigml->CreateFontTexture(static_cast<uint32_t>(_textureId));
        
        CGMLBuffer buffer(1024u, _argBuffer);
        buffer.Write<uint32_t>(imguigml->GetFontWidth());
        buffer.Write<uint32_t>(imguigml->GetFontHeight());
        return 1.0;
    }

    DLL_API double GetFontTexture(char *_fontBuffer) {
        CheckInit();
        imguigml->WriteFont(_fontBuffer);
        return 1.0;
    }

    DLL_API double UpdateInput(char *_inputBuffer) {
        CheckInit();
        CGMLBuffer buffer(1024, _inputBuffer);
        imguigml->UpdateInput(buffer);
        return 1.0;
    }

    DLL_API double BeginStep(double _deltaTime) {
        CheckInit();
        imguigml->BeginStep(_deltaTime);
        return 1.0;
    }

    DLL_API double EndStep() {
        CheckInit();
        imguigml->EndStep();
        return 1.0;
    }

    DLL_API double WaitForRender() {
        CheckInit();
        imguigml->WaitForBuffer();
        return 1.0;
    }
}
