#pragma once
#include "types.h"

class TaskRunner {
public:
    struct Task;
    using TaskFunction = std::function<void(Task&)>;
    
    struct Task {
        Task(TaskRunner& _runner, size_t _index);
        virtual ~Task() { Cancel(); }

        void SetReady(std::function<void(Task&)>&& _func);
        void Cancel();
        
        bool Cancelled() const { return runner.mShutdown || cancelled; }
        
        size_t index;
        std::atomic<bool> active;
        std::atomic<bool> taskReady;
        std::atomic<bool> cancelled;
        std::function<void(Task&)> taskFunc;
        TaskRunner& runner;
        SpinLock taskLock;
    };

public:
    virtual ~TaskRunner() { mShutdown = true; for (auto& task : mTasks) { task->Cancel(); } }
    Task& Run(std::function<void(TaskRunner::Task&)>&& _func);

private:
    std::atomic<bool>                  mShutdown = { false };
    std::stack<size_t>                 mFreeTasks;
    std::vector<std::unique_ptr<Task>> mTasks;

    friend struct Task;
};