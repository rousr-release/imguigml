#include "imguigml_wrappers.h"

extern "C" {
    ///////////
    // Tooltips
    ///////////

    ////
    // IMGUI_API void          SetTooltip(const char* fmt, ...) IM_FMTARGS(1);
    // IMGUI_API void          SetTooltipV(const char* fmt, va_list args) IM_FMTLIST(1);
    //  set text tooltip under mouse-cursor, typically use with ImGui::IsItemHovered(). overidde any previous call to SetTooltip().
    IMGML SetTooltip() {
        CheckImGML_In();
        ImGui::SetTooltip("%s", In_ImGML_String().c_str());
        return 1.0;
    }

    ////
    // IMGUI_API void          BeginTooltip();
    //  begin/append a tooltip window. to create full-featured tooltip (with any kind of contents).
    IMGML BeginTooltip() {
        CheckImGML();
        ImGui::BeginTooltip();
        return 1.0;
    }

    ////
    // IMGUI_API void          EndTooltip();
    IMGML EndTooltip() {
        CheckImGML();
        ImGui::EndTooltip();
        return 1.0;
    }
}