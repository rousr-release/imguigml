#include "imguigml_wrappers.h"

extern "C" {

    ////
    // IMGUI_API ImGuiStyle&   GetStyle();
    IMGML GetStyle() {
        CheckImGML();
        ImGML_NotImplemented();
    }

    ////////////////////
    // Demo/Debug/Info
    ////////////////////

    ////
    // IMGUI_API void          ShowDemoWindow(bool* p_open = NULL);
    // create demo/test window (previously called ShowTestWindow). demonstrate most ImGui features. call this to learn about the library! try to make it always available in your application!
    IMGML ShowDemoWindow() {
        bool open(false);
        bool *popen(nullptr);

        CheckImGML_InOut();
        if (in.NextType<int8_t>()) { open = In_ImGML_Bool(); popen = &open; }
        else In_ImGML_Skip();
        ImGui::ShowDemoWindow(popen);
        if (popen != nullptr)
            Out_ImGML_Bool(open);

        return 1.0;
    }

    ////
    // IMGUI_API void          ShowMetricsWindow(bool* p_open = NULL);     
    //  create metrics window. display ImGui internals: draw commands (with individual draw calls and vertices), window list, basic internal state, etc.
    IMGML ShowMetricsWindow() {
        bool open(false);
        bool *popen(nullptr);

        CheckImGML_InOut();
        if (in.NextType<int8_t>()) { open = In_ImGML_Bool(); popen = &open; }
        else In_ImGML_Skip();
        ImGui::ShowMetricsWindow(popen);
        if (popen != nullptr)
            Out_ImGML_Bool(open);
        
        return 1.0;
    }

    ////
    // IMGUI_API void          ShowFontSelector(const char* label);
    IMGML ShowFontSelector() {
        CheckImGML_In();
		auto label(In_ImGML_String());
        ImGui::ShowFontSelector(label.c_str());
        return 1.0;
    }

    ////
    // IMGUI_API void          ShowUserGuide();                            
    //  add basic help/info block (not a window): how to manipulate ImGui as a end-user (mouse/keyboard controls).
    IMGML ShowUserGuide() {
        CheckImGML();
        ImGui::ShowUserGuide();
        return 1.0;
    }

    ////////////////////
    // Window
    ////////////////////
    
    ////
    // IMGUI_API bool          Begin(const char* name, bool* p_open = NULL, ImGuiWindowFlags flags = 0); 
    //  push window to the stack and start appending to it. see .cpp for details. return false when window is collapsed, so you can early out in your code. 
    //  'bool* p_open' creates a widget on the upper-right to close the window (which sets your bool to false).
    IMGML Begin() {
        bool open(false);
        bool *popen(nullptr);
        
        CheckImGML_InOut();
        auto label(In_ImGML_String());
        if (in.NextType<int8_t>()) { open = In_ImGML_Bool(); popen = &open; } else In_ImGML_Skip();
        ImGuiWindowFlags flags(static_cast<ImGuiWindowFlags>(In_ImGML_IntOpt(0)));
        Out_ImGML_Bool(ImGui::Begin(label.c_str(), popen, flags));
        if (popen != nullptr)
            Out_ImGML_Bool(open);
        return 1.0;
    }

    ////
    // IMGUI_API void          End();                                                                                                                      
    //  finish appending to current window, pop it off the window stack.
    IMGML End() {
        CheckImGML();
        ImGui::End();
        return 1.0;
    }
    
    ////
    // BeginChild
    // IMGUI_API bool          BeginChild(const char* str_id, const ImVec2& size = ImVec2(0, 0), bool border = false, ImGuiWindowFlags extra_flags = 0);    
    // IMGUI_API bool          BeginChild(ImGuiID id, const ImVec2& size = ImVec2(0, 0), bool border = false, ImGuiWindowFlags extra_flags = 0);
    //   begin a scrolling region. size==0.0f: use remaining window size, size<0.0f: use remaining window size minus abs(size). size>0.0f: fixed size. each axis can use a different mode, e.g. ImVec2(0,400).
    IMGML BeginChild() {
        CheckImGML_InOut();
        
        In_ImGML_Skip();
        auto x(In_ImGML_FloatOpt(0.0f));
        auto y(In_ImGML_FloatOpt(0.0f));
        auto border(In_ImGML_BoolOpt(false));
        auto extraFlags(In_ImGML_IntOpt(0));

        if (in.IsType<std::string>(0)) {
            auto strId(in.Val<std::string>(0));
            Out_ImGML_Bool(ImGui::BeginChild(strId.c_str(), ImVec2(x, y), border, extraFlags));
        } else {
            auto id(in.Val<std::uint32_t>(0));
            Out_ImGML_Bool(ImGui::BeginChild(id, ImVec2(x, y), border, extraFlags));
        }

        return 1.0;
    }
    
    ////
    //IMGUI_API void          EndChild();
    IMGML EndChild() {
        CheckImGML();
        ImGui::EndChild();
        return 1.0;
    }

    ////
    // IMGUI_API ImVec2        GetContentRegionMax();                                              
    //  current content boundaries (typically window boundaries including scrolling, or current column boundaries), in windows coordinates
    IMGML GetContentRegionMax() {
        CheckImGML_Out();
        auto regionMax(ImGui::GetContentRegionMax());
        Out_ImGML_Float(regionMax.x);
        Out_ImGML_Float(regionMax.y);
        return 1.0;
    }

    ////
    // IMGUI_API ImVec2        GetContentRegionAvail();                                            
    //  == GetContentRegionMax() - GetCursorPos()
    IMGML GetContentRegionAvail() {
        CheckImGML_Out();
        auto regionAvail(ImGui::GetContentRegionAvail());
        Out_ImGML_Float(regionAvail.x);
        Out_ImGML_Float(regionAvail.y);
        return 1.0;
    }

    ////
    // IMGUI_API float         GetContentRegionAvailWidth();                                    
    IMGML GetContentRegionAvailWidth() {
        CheckImGML_Out();
        Out_ImGML_Float(ImGui::GetContentRegionAvailWidth());
        return 1.0;
    }
    
    ////
    // IMGUI_API ImVec2        GetWindowContentRegionMin();                                        
    //  content boundaries min (roughly (0,0)-Scroll), in window coordinates
    IMGML GetWindowContentRegionMin() {
        CheckImGML_Out();
        auto windowContentRegionMin(ImGui::GetWindowContentRegionMin());
        Out_ImGML_Float(windowContentRegionMin.x);
        Out_ImGML_Float(windowContentRegionMin.y);
        return 1.0;
    }
    
    ////
    // IMGUI_API ImVec2        GetWindowContentRegionMax();                                        
    //  content boundaries max (roughly (0,0)+Size-Scroll) where Size can be override with SetNextWindowContentSize(), in window coordinates
    IMGML GetWindowContentRegionMax() {
        CheckImGML_Out();
        auto windowContentRegionMax(ImGui::GetWindowContentRegionMax());
        Out_ImGML_Float(windowContentRegionMax.x);
        Out_ImGML_Float(windowContentRegionMax.y);
        return 1.0;
    }
    
    ////
    // IMGUI_API float         GetWindowContentRegionWidth();                                     
    IMGML GetWindowContentRegionWidth() {
        CheckImGML_Out();
        Out_ImGML_Double(ImGui::GetWindowContentRegionWidth());
        return 1.0;
    }

    ////
    // IMGUI_API ImDrawList*   GetWindowDrawList();                                                
    //  get rendering command-list if you want to append your own draw primitives
    IMGML GetWindowDrawList() {
        // todo: implement returning draw lists
        ImGML_NotImplemented();
    }

    ////
    // IMGUI_API ImVec2        GetWindowPos();
    //  get current window position in screen space (useful if you want to do your own drawing via the DrawList api)
    IMGML GetWindowPos() {
        CheckImGML_Out();
        auto pos(ImGui::GetWindowPos());
        Out_ImGML_Float(pos.x);
        Out_ImGML_Float(pos.y);
        return 1.0;
    }

    ////
    // IMGUI_API ImVec2        GetWindowSize();
    //  get current window size
    IMGML GetWindowSize() {
        CheckImGML_Out();
        auto size(ImGui::GetWindowSize());
        Out_ImGML_Float(size.x);
        Out_ImGML_Float(size.y);
        return 1.0;
    }

    ////
    // IMGUI_API float         GetWindowWidth();
    IMGML GetWindowWidth() {
        CheckImGML_Out();
        Out_ImGML_Float(ImGui::GetWindowWidth());
        return 1.0;
    }

    ////
    // IMGUI_API float         GetWindowHeight();
    IMGML GetWindowHeight() {
        CheckImGML_Out();
        Out_ImGML_Float(ImGui::GetWindowHeight());
        return 1.0;
    }

    ////
    // IMGUI_API bool          IsWindowCollapsed();
    IMGML IsWindowCollapsed() {
        CheckImGML_Out();
        Out_ImGML_Bool(ImGui::IsWindowCollapsed());
        return 1.0;
    }

    ////
    // IMGUI_API bool          IsWindowAppearing();
    IMGML IsWindowAppearing() {
        CheckImGML_Out();
        Out_ImGML_Bool(ImGui::IsWindowAppearing());
        return 1.0;
    }

    ////
    // IMGUI_API void          SetWindowFontScale(float scale);
    //  per-window font scale. Adjust IO.FontGlobalScale if you want to scale all windows
    IMGML SetWindowFontScale() {
        // todo: support Adjust IO.FontGlobalScale if you want to scale all windows
        CheckImGML_In();
        ImGui::SetWindowFontScale(In_ImGML_Float());
        return 1.0;
    }


    ////
    // IMGUI_API void          SetNextWindowPos(const ImVec2& pos, ImGuiCond cond = 0, const ImVec2& pivot = ImVec2(0, 0));
    //  set next window position. call before Begin(). use pivot=(0.5f,0.5f) to center on given point, etc.
    IMGML SetNextWindowPos() {
        CheckImGML_In();
        float x(In_ImGML_Float());
        float y(In_ImGML_Float());
        ImGuiCond cond(In_ImGML_IntOpt(0));
        float px(In_ImGML_FloatOpt(0.0f));
        float py(In_ImGML_FloatOpt(0.0f));
        ImGui::SetNextWindowPos(ImVec2(x, y), cond, ImVec2(px, py));
        return 1.0;
    }

    ////
    // IMGUI_API void          SetNextWindowSize(const ImVec2& size, ImGuiCond cond = 0);
    //  set next window size. set axis to 0.0f to force an auto-fit on this axis. call before Begin()
    IMGML SetNextWindowSize() {
        CheckImGML_In();
        float x(In_ImGML_Float());
        float y(In_ImGML_Float());
        ImGuiCond cond(In_ImGML_IntOpt(0));
        ImGui::SetNextWindowSize(ImVec2(x, y), cond);
        return 1.0;
    }

    ////
    // IMGUI_API void          SetNextWindowSizeConstraints(const ImVec2& size_min, const ImVec2& size_max, ImGuiSizeConstraintCallback custom_callback = NULL, void* custom_callback_data = NULL);
    //  set next window size limits. use -1,-1 on either X/Y axis to preserve the current size. 
    //todo: Use callback to apply non-trivial programmatic constraints.
    IMGML SetNextWindowSizeConstraints() {
        CheckImGML_In();
        float minx(In_ImGML_Float());
        float miny(In_ImGML_Float());
        float maxx(In_ImGML_Float());
        float maxy(In_ImGML_Float());

        // todo: support callback
        ImGui::SetNextWindowSizeConstraints(ImVec2(minx, miny), ImVec2(maxx, maxy), [](ImGuiSizeCallbackData* _data) {
            
            

        }, nullptr);

        return 1.0;
    }

    ////
    // IMGUI_API void          SetNextWindowContentSize(const ImVec2& size);
    //  set next window content size (enforce the range of scrollbars). set axis to 0.0f to leave it automatic. call before Begin()
    IMGML SetNextWindowContentSize() {
        CheckImGML_In();
        float x(In_ImGML_Float());
        float y(In_ImGML_Float());
        ImGui::SetNextWindowContentSize(ImVec2(x, y));
        return 1.0;
    }

    ////
    // IMGUI_API void          SetNextWindowCollapsed(bool collapsed, ImGuiCond cond = 0);
    //  set next window collapsed state. call before Begin()
    IMGML SetNextWindowCollapsed() {
        CheckImGML_In();
        bool collapsed(In_ImGML_Bool());
        ImGuiCond cond(In_ImGML_IntOpt(0));
        ImGui::SetNextWindowCollapsed(collapsed, cond);
        return 1.0;
    }

    ////
    // IMGUI_API void          SetNextWindowFocus();
    //  set next window to be focused / front-most. call before Begin()
    IMGML SetNextWindowFocus() {
        CheckImGML();
        ImGui::SetNextWindowFocus();
        return 1.0;
    }

    ////
    // IMGUI_API void          SetWindowPos(const char* name, const ImVec2& pos, ImGuiCond cond = 0);
    //  set named window position.
    IMGML ImGuiSetWindowPos() {
        CheckImGML_In();
        std::string name(In_ImGML_String());
        float x(In_ImGML_Float());
        float y(In_ImGML_Float());
        ImGuiCond cond(In_ImGML_IntOpt(0));
        ImGui::SetWindowPos(name.c_str(), ImVec2(x, y), cond);
        return 1.0;
    }

    ////
    // IMGUI_API void          SetWindowSize(const char* name, const ImVec2& size, ImGuiCond cond = 0);
    //  set named window size. set axis to 0.0f to force an auto-fit on this axis.
    IMGML SetWindowSize() {
        CheckImGML_In();
        std::string name(In_ImGML_String());
        float x(In_ImGML_Float());
        float y(In_ImGML_Float());
        ImGuiCond cond(In_ImGML_IntOpt(0));
        ImGui::SetWindowSize(name.c_str(), ImVec2(x, y), cond);
        return 1.0;
    }

    ////
    // IMGUI_API void          SetWindowCollapsed(const char* name, bool collapsed, ImGuiCond cond = 0);
    //  set named window collapsed state
    IMGML SetWindowCollapsed() {
        CheckImGML_In();
        std::string name(In_ImGML_String());
        bool collapsed(In_ImGML_Bool());
        ImGuiCond cond(In_ImGML_IntOpt(0));
        ImGui::SetWindowCollapsed(name.c_str(), collapsed, cond);
        return 1.0;
    }

    ////
    // IMGUI_API void          SetWindowFocus(const char* name);
    //  set named window to be focused / front-most. use NULL to remove focus.
    IMGML SetWindowFocus() {
        CheckImGML_In();
        std::string name(In_ImGML_String());
        ImGui::SetWindowFocus(name.c_str());
        return 1.0;
    }


    ////
    // IMGUI_API float         GetScrollX();
    //  get scrolling amount [0..GetScrollMaxX()]
    IMGML GetScrollX() {
        CheckImGML_Out();
        Out_ImGML_Float(ImGui::GetScrollX());
        return 1.0;
    }

    ////
    // IMGUI_API float         GetScrollY();
    //  get scrolling amount [0..GetScrollMaxY()]
    IMGML GetScrollY() {
        CheckImGML_Out();
        Out_ImGML_Float(ImGui::GetScrollY());
        return 1.0;
    }

    ////
    // IMGUI_API float         GetScrollMaxX();
    //  get maximum scrolling amount ~~ ContentSize.X - WindowSize.X
    IMGML GetScrollMaxX() {
        CheckImGML_Out();
        Out_ImGML_Float(ImGui::GetScrollMaxX());
        return 1.0;
    }

    ////
    // IMGUI_API float         GetScrollMaxY();
    //  get maximum scrolling amount ~~ ContentSize.Y - WindowSize.Y
    IMGML GetScrollMaxY() {
        CheckImGML_Out();
        Out_ImGML_Float(ImGui::GetScrollMaxY());
        return 1.0;
    }

    ////
    // IMGUI_API void          SetScrollX(float scroll_x);
    //  set scrolling amount [0..GetScrollMaxX()]
    IMGML SetScrollX() {
        CheckImGML_In();
        ImGui::SetScrollX(In_ImGML_Float());
        return 1.0;
    }

    ////
    // IMGUI_API void          SetScrollY(float scroll_y);
    //  set scrolling amount [0..GetScrollMaxY()]
    IMGML SetScrollY() {
        CheckImGML_In();
        ImGui::SetScrollY(In_ImGML_Float());
        return 1.0;
    }

    ////
    // IMGUI_API void          SetScrollHereX()
    //  adjust scrolling amount to make current cursor position visible. center_y_ratio=0.0: top, 0.5: center, 1.0: bottom.
    IMGML SetScrollHereX() {
        CheckImGML_In();
		ImGui::SetScrollHereX();
        return 1.0;
    }

	////
	// IMGUI_API void          SetScrollHereX()
	//  adjust scrolling amount to make current cursor position visible. center_y_ratio=0.0: top, 0.5: center, 1.0: bottom.
	IMGML SetScrollHereY() {
		CheckImGML_In();
		ImGui::SetScrollHereY();
		return 1.0;
	}

    ////
    // IMGUI_API void          SetScrollFromPosY(float pos_y, float center_y_ratio = 0.5f);
    //  adjust scrolling amount to make given position valid. use GetCursorPos() or GetCursorStartPos()+offset to get valid positions.
    IMGML SetScrollFromPosY() {
        CheckImGML_In();
        float posY(In_ImGML_Float());
        float centerYRatio(In_ImGML_FloatOpt(0.5f));
        ImGui::SetScrollFromPosY(posY, centerYRatio);
        return 1.0;
    }

    ////
    // IMGUI_API void          SetKeyboardFocusHere(int offset = 0);
    //  focus keyboard on the next widget. Use positive 'offset' to access sub components of a multiple component widget. Use -1 to access previous widget.
    IMGML SetKeyboardFocusHere() {
        CheckImGML_In();
        ImGui::SetKeyboardFocusHere(In_ImGML_IntOpt(0));
        return 1.0;
    }

    ////
    // IMGUI_API void          SetStateStorage(ImGuiStorage* tree);
    //  replace tree state storage with our own (if you want to manipulate it yourself, typically clear subsection of it)
    IMGML SetStateStorage() {
        // todo: implement
        ImGML_NotImplemented();
    }

    ////
    // IMGUI_API ImGuiStorage* GetStateStorage();
    IMGML GetStateStorage() {
        // todo: implement
        ImGML_NotImplemented();
    }

    ////
    // IMGUI_API bool          BeginChildFrame(ImGuiID id, const ImVec2& size, ImGuiWindowFlags extra_flags = 0);    
    //  helper to create a child window / scrolling region that looks like a normal widget frame
    IMGML BeginChildFrame() {
        CheckImGML_InOut();
        auto _id(In_ImGML_Uint());
        auto x(In_ImGML_Float());
        auto y(In_ImGML_Float());
        auto flags(In_ImGML_IntOpt(0));
        Out_ImGML_Bool(ImGui::BeginChildFrame(_id, ImVec2(x, y), flags));

        return 1.0;
    }
 
    ////
    //IMGUI_API void          EndChildFrame();
    IMGML EndChildFrame() {
        CheckImGML();
        ImGui::EndChildFrame();
        return 1.0;
    }
}
