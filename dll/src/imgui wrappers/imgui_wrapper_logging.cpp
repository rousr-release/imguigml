#include "imguigml_wrappers.h"

extern "C" {

    ////////////
    // Logging: all text output from interface is redirected to tty/file/clipboard. By default, tree nodes are automatically opened during logging.
    ////////////

    // IMGUI_API void          LogToTTY(int max_depth = -1);    
    //  start logging to tty
    IMGML LogToTTY() {
        CheckImGML_In();
        ImGui::LogToTTY(In_ImGML_IntOpt(-1));
        return 1.0;
    }
    // IMGUI_API void          LogToFile(int max_depth = -1, const char* filename = NULL);  
    //  start logging to file
    IMGML LogToFile() {
        CheckImGML_In();
        std::string strFile;
        const char *file(nullptr);
        
        auto maxDepth(In_ImGML_IntOpt(-1));
        if (in.Count() >= 2) {
            strFile = In_ImGML_String();
            file = strFile.c_str();
        }
        
        ImGui::LogToFile(maxDepth, file);
        return 1.0;
    }
    // IMGUI_API void          LogToClipboard(int max_depth = -1);  
    //  start logging to OS clipboard
    IMGML LogToClipboard() {
        CheckImGML_In();
        ImGui::LogToClipboard(In_ImGML_IntOpt(-1));
        return 1.0;
    }
    // IMGUI_API void          LogFinish();                                         
    //  stop logging (close file, etc.)
    IMGML LogFinish() {
        CheckImGML();
        ImGui::LogFinish();
        return 1.0;
    }
    // IMGUI_API void          LogButtons();          
    //  helper to display buttons for logging to tty/file/clipboard
    IMGML LogButtons() {
        CheckImGML();
        ImGui::LogButtons();
        return 1.0;
    }
    // IMGUI_API void          LogText(const char* fmt, ...) IM_FMTARGS(1);        
    //  pass text data straight to log (without being displayed)
    IMGML LogText() {
        CheckImGML_In();
        ImGui::LogText("%s", In_ImGML_String().c_str());
        return 1.0;
    }
}