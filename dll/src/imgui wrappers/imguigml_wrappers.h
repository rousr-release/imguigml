#pragma once

#include "imguigml.h"
#include "imgui/imgui.h"

#include "rousrGML/GMLTypedBuffer.h"
#include "rousrGML/GMLBuffer.h"

#if defined(_WIN32)
#include <windows.h>
#endif

#define IMGML                     DLL_API double
#define ImGML_NotImplemented()    return -1.0;

#define CheckImGML()         const auto& imguigml(ImGuiGML::Instance()); if (imguigml == nullptr) return -1.0; auto& io(ImGui::GetIO());
#define CheckImGML_InOut()   const auto& imguigml(ImGuiGML::Instance()); if (imguigml == nullptr) return -1.0; auto& io(ImGui::GetIO()); CGMLTypedBuffer in(imguigml->WrapperBuffer()); CGMLBuffer& out(imguigml->WrapperBuffer()); 
#define CheckImGML_In()      const auto& imguigml(ImGuiGML::Instance()); if (imguigml == nullptr) return -1.0; auto& io(ImGui::GetIO()); CGMLTypedBuffer in(imguigml->WrapperBuffer());
#define CheckImGML_Out()     const auto& imguigml(ImGuiGML::Instance()); if (imguigml == nullptr) return -1.0; auto& io(ImGui::GetIO()); CGMLBuffer& out(imguigml->WrapperBuffer()); 

#define CheckImGML_NoReturn()         const auto& imguigml(ImGuiGML::Instance()); if (imguigml == nullptr) return; auto& io(ImGui::GetIO());
#define CheckImGML_InOut_NoReturn()   const auto& imguigml(ImGuiGML::Instance()); if (imguigml == nullptr) return; auto& io(ImGui::GetIO()); CGMLTypedBuffer in(imguigml->WrapperBuffer()); CGMLBuffer& out(imguigml->WrapperBuffer()); 
#define CheckImGML_In_NoReturn()      const auto& imguigml(ImGuiGML::Instance()); if (imguigml == nullptr) return; auto& io(ImGui::GetIO()); CGMLTypedBuffer in(imguigml->WrapperBuffer());
#define CheckImGML_Out_NoReturn()     const auto& imguigml(ImGuiGML::Instance()); if (imguigml == nullptr) return; auto& io(ImGui::GetIO()); CGMLBuffer& out(imguigml->WrapperBuffer()); 

#define In_ImGML_Float()       (in.NextVal<float>())
#define In_ImGML_FloatOpt(v)   (in.NextVal<float>(v))
                               
#define In_ImGML_Int()         (in.NextVal<int32_t>())
#define In_ImGML_IntOpt(v)     (in.NextVal<int32_t>(v))
                               
#define In_ImGML_Uint()        (in.NextVal<uint32_t>())
#define In_ImGML_UintOpt(v)    (in.NextVal<uint32_t>(v))
                               
#define In_ImGML_Bool()        (in.NextVal<int8_t>() == 1)
#define In_ImGML_BoolOpt(v)    (in.NextVal<int8_t>(v ? 1 : 0) == 1)
                               
#define In_ImGML_String()      (in.NextVal<std::string>())
#define In_ImGML_StringOpt(v)  (in.NextVal<std::string>(v))

#define In_ImGML_Ubyte()       (in.NextVal<uint8_t>())
#define In_ImGML_UbyteOpt(v)   (in.NexxVal<uint8_t>(v));

#define In_ImGML_Double()      (in.NextVal<double>())
#define In_ImGML_DoubleOpt(v)  (in.NextVal<double>(v))

#define In_ImGML_Skip()        (in.NextVal())

#define Out_ImGML_Float(v)     (out.Write<float>(v))
#define Out_ImGML_Double(v)    (out.Write<double>(v))
#define Out_ImGML_Int(v)       (out.Write<int32_t>(v))
#define Out_ImGML_Uint(v)      (out.Write<uint32_t>(v))
#define Out_ImGML_Bool(v)      (out.Write<int8_t>((v) ? 1 : 0))
#define Out_ImGML_String(v)    (out.Write<std::string>(v))
#define Out_ImGML_Ubyte(v)     (out.Write<uint8_t>(v))

#if defined(_WIN32) 
inline std::wstring widen(std::string str) {
    if (str.empty()) return L"";
    size_t wchar_count = str.size() + 1;
    std::vector<wchar_t> buf(wchar_count);
    return std::wstring{ buf.data(), (size_t)MultiByteToWideChar(CP_UTF8, 0, str.c_str(), -1, buf.data(), (int)wchar_count) };
}

inline std::string narrow(std::wstring wstr) {
    if (wstr.empty()) return "";
    int nbytes = WideCharToMultiByte(CP_UTF8, 0, wstr.c_str(), (int)wstr.length(), nullptr, 0, nullptr, nullptr);
    std::vector<char> buf(nbytes);
    return std::string{ buf.data(), (size_t)WideCharToMultiByte(CP_UTF8, 0, wstr.c_str(), (int)wstr.length(), buf.data(), nbytes, nullptr, nullptr) };
}
#endif

extern "C" DLL_API const char *environment_get_variable(const char *name);
extern "C" DLL_API double environment_set_variable(const char *name, const char *value);

extern "C" DLL_API const char *imguigml_get_fontfiles();
extern "C" DLL_API double imguigml_set_fontfiles(const char *fontfiles);

extern "C" DLL_API double imguigml_get_fontsize();
extern "C" DLL_API double imguigml_set_fontsize(double fontsize);

enum class EImGuiGMLCallbacks {
    None = 0,
    
    Done = 1,

    TextCharAlways,
    TextCharFilter,
    TextComplete,
    TextHistory,

    Num,
};
