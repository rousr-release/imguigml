#include "imguigml_wrappers.h"

#include "rousrGML/GMLSharedCallstack.h"

namespace {
    const uint32_t MaxImGuiBuff(4096);
    struct CallData { CallData(GMLSharedCallstack::GMLCall* _call=nullptr) : call(_call) { ; } GMLSharedCallstack::GMLCall* call; };

    bool ShouldUseCallback(uint32_t _flags) {
        return (((_flags & ImGuiInputTextFlags_CallbackAlways)    != 0) ||
               ((_flags & ImGuiInputTextFlags_CallbackCharFilter) != 0) ||
               ((_flags & ImGuiInputTextFlags_CallbackCompletion) != 0) ||
               ((_flags & ImGuiInputTextFlags_CallbackHistory)    != 0)); 
    }

    int TextCallback(ImGuiInputTextCallbackData* _data) {
        auto& _call(*reinterpret_cast<CallData*>(_data->UserData)->call);
        auto& data(*_data);
        
        auto& out(_call.Buffer());
        out.Write<int32_t>(data.EventFlag);        // One of ImGuiInputTextFlags_Callback* // Read-only
        out.Write<int32_t>(data.Flags);            // What user passed to InputText()      // Read-only
        
        int ret(0);
        switch (data.EventFlag) {
        case ImGuiInputTextFlags_CallbackAlways:        // Call user function every time. User code may query cursor position, modify text buffer.
        case ImGuiInputTextFlags_CallbackCompletion:    // Call user function on pressing TAB (for completion handling)
        case ImGuiInputTextFlags_CallbackHistory:       // Call user function on pressing Up/Down arrows (for history handling)
        {                                               // If you modify the buffer contents make sure you update 'BufTextLen' and set 'BufDirty' to true.
            out.Write<int32_t>(data.EventKey);         // Key pressed (Up/Down/TAB)            // Read-only
            out.Write<std::string>(data.Buf);          // Current text buffer                  // Read-write (pointed data only, can't replace the actual pointer)
            out.Write<int32_t>(data.BufSize);          // Maximum text length in bytes         // Read-only
            out.Write<int32_t>(data.CursorPos);        //                                      // Read-write
            out.Write<int32_t>(data.SelectionStart);   //                                      // Read-write (== to SelectionEnd when no selection)
            out.Write<int32_t>(data.SelectionEnd);     //                                      // Read-write
            _call.YieldToGML();

            CGMLTypedBuffer in(_call.Buffer());
            ret = in.NextVal<int32_t>();

            std::string _buffText(in.NextVal<std::string>());
            data.CursorPos      = in.NextVal<int32_t>();
            data.SelectionStart = in.NextVal<int32_t>();
            data.SelectionEnd   = in.NextVal<int32_t>();
            
            data.BufDirty = _buffText != std::string(data.Buf);
            if (data.BufDirty) {
                memset(data.Buf, 0, data.BufSize);
                std::memcpy(data.Buf, _buffText.c_str(), std::min<size_t>(_buffText.length(), data.BufSize));
                data.BufTextLen = static_cast<int>(_buffText.length());
            }
        } break;
        case ImGuiInputTextFlags_CallbackCharFilter:    // Call user function to filter character. Modify data->EventChar to replace/filter input, or return 1 to discard character.
        {
            out.Write<uint16_t>(data.EventChar);       // Character input                      // Read-write (replace character or set to zero)
            _call.YieldToGML();

            CGMLTypedBuffer in(_call.Buffer());
            ret = in.NextVal<int32_t>();
            data.EventChar = in.NextVal<uint16_t>();
        } break;
        default: break;
        }
        
        return ret;
    }

    template <typename TFunc>
    double InputCommand(uint32_t _flags, TFunc _boundFunc) {
        
        ImGuiContext* ctx(ImGui::GetCurrentContext());
        if (ShouldUseCallback(_flags))
            return GMLSharedCallstack::Call([_boundFunc, ctx](GMLSharedCallstack::GMLCall& _call) { CallData callData(&_call); ImGui::SetCurrentContext(ctx);  _boundFunc(callData); });
        
        CallData callData;
        _boundFunc(callData);
        return -1.0;
    }
}

extern "C" {
    ////
    // Widgets: Input with Keyboard
    ////
    
    ////
    //IMGUI_API bool          InputText(const char* label, char* buf, size_t buf_size, ImGuiInputTextFlags flags = 0, ImGuiTextEditCallback callback = NULL, void* user_data = NULL);
    IMGML InputText() {
        CheckImGML_InOut();
        auto label(In_ImGML_String());
        auto text(In_ImGML_String());
        auto length(In_ImGML_Uint());
        auto flags(In_ImGML_IntOpt(0));
        
        return InputCommand(flags, [label, text, length, flags](CallData& callData) {
            char buf[MaxImGuiBuff]; // todo: write string container
            memset(buf, 0, MaxImGuiBuff);
            strncpy_s(buf, text.c_str(), text.length());
            
            CheckImGML_InOut_NoReturn();
            Out_ImGML_Bool(ImGui::InputText(label.c_str(), buf, static_cast<size_t>(length), flags, TextCallback, reinterpret_cast<void*>(&callData)));
            Out_ImGML_String(buf);
        });
    }

    ////
    //IMGUI_API bool          InputTextMultiline(const char* label, char* buf, size_t buf_size, const ImVec2& size = ImVec2(0, 0), ImGuiInputTextFlags flags = 0, ImGuiTextEditCallback callback = NULL, void* user_data = NULL);
    IMGML InputTextMultiline() {
        CheckImGML_InOut();
        auto label(In_ImGML_String());
        auto text(In_ImGML_String());
        auto length(In_ImGML_Uint());
        auto x(In_ImGML_FloatOpt(0.0f));
        auto y(In_ImGML_FloatOpt(0.0f));
        auto flags(In_ImGML_IntOpt(0));
       
        return InputCommand(flags, [label, text, length, x, y, flags](CallData& callData) {
            char buf[MaxImGuiBuff]; // todo: write string container
            memset(buf, 0, MaxImGuiBuff);
            strncpy_s(buf, text.c_str(), text.length());

            CheckImGML_InOut_NoReturn();
            Out_ImGML_Bool(ImGui::InputTextMultiline(label.c_str(), buf, static_cast<size_t>(length), ImVec2(x, y), flags, TextCallback, reinterpret_cast<void*>(&callData)));
            Out_ImGML_String(buf);
        });
    }
        
    ////
    //IMGUI_API bool          InputFloat(const char* label, float* v, float step = 0.0f, float step_fast = 0.0f, int decimal_precision = -1, ImGuiInputTextFlags extra_flags = 0);
    IMGML InputFloat() {
        CheckImGML_InOut();
        auto label(In_ImGML_String());
        auto v(In_ImGML_Float());
        auto step(In_ImGML_FloatOpt(0.0f));
        float stepFast(In_ImGML_FloatOpt(0.0f));
        int32_t decimalPrecision(In_ImGML_IntOpt(-1));
        int32_t extraFlags(In_ImGML_IntOpt(0));

		std::string format("%." + std::to_string(decimalPrecision) + "F");
        Out_ImGML_Bool(ImGui::InputFloat(label.c_str(), &v, step, stepFast, format.c_str(), extraFlags));
        Out_ImGML_Float(v);
        return 1.0;
    }

    ////
    //IMGUI_API bool          InputFloat2(const char* label, float v[2], int decimal_precision = -1, ImGuiInputTextFlags extra_flags = 0);
    IMGML InputFloat2() {
        const uint8_t Elems(2);
        float v[Elems];
       
        CheckImGML_InOut();
        auto label(In_ImGML_String());
        for (uint8_t i(0); i < Elems; ++i) v[i] = In_ImGML_Float();
        int32_t decimalPrecision(In_ImGML_IntOpt(-1));
        int32_t extraFlags(In_ImGML_IntOpt(0));
		
		std::string format("%." + std::to_string(decimalPrecision) + "F");
		Out_ImGML_Bool(ImGui::InputFloat2(label.c_str(), v, format.c_str(), extraFlags));
        for (uint8_t i(0); i < Elems; ++i) Out_ImGML_Float(v[i]);
        return 1.0;
    }

    ////
    //IMGUI_API bool          InputFloat3(const char* label, float v[3], int decimal_precision = -1, ImGuiInputTextFlags extra_flags = 0);
    IMGML InputFloat3() {
        const uint8_t Elems(3);
        float v[Elems];

        CheckImGML_InOut();
        auto label(In_ImGML_String());
        for (uint8_t i(0); i < Elems; ++i) v[i] = In_ImGML_Float();
        int32_t decimalPrecision(In_ImGML_IntOpt(-1));
        int32_t extraFlags(In_ImGML_IntOpt(0));
		
		std::string format("%." + std::to_string(decimalPrecision) + "F");
        Out_ImGML_Bool(ImGui::InputFloat3(label.c_str(), v, format.c_str(), extraFlags));
        for (uint8_t i(0); i < Elems; ++i) Out_ImGML_Float(v[i]);
        return 1.0;
    }

    ////
    //IMGUI_API bool          InputFloat4(const char* label, float v[4], int decimal_precision = -1, ImGuiInputTextFlags extra_flags = 0);
    IMGML InputFloat4() {
        const uint8_t Elems(4);
        float v[Elems];

        CheckImGML_InOut();
        auto label(In_ImGML_String());
        for (uint8_t i(0); i < Elems; ++i) v[i] = In_ImGML_Float();
        int32_t decimalPrecision(In_ImGML_IntOpt(-1));
        int32_t extraFlags(In_ImGML_IntOpt(0));
		
		std::string format("%." + std::to_string(decimalPrecision) + "F");
        Out_ImGML_Bool(ImGui::InputFloat4(label.c_str(), v, format.c_str(), extraFlags));
        for (uint8_t i(0); i < Elems; ++i) Out_ImGML_Float(v[i]);
        return 1.0;
    }

    ////
    //IMGUI_API bool          InputInt(const char* label, int* v, int step = 1, int step_fast = 100, ImGuiInputTextFlags extra_flags = 0);
    IMGML InputInt() {
        CheckImGML_InOut();
        auto label(In_ImGML_String());
        auto v(In_ImGML_Int());
        auto step(In_ImGML_IntOpt(1));
        auto stepFast(In_ImGML_IntOpt(100));
        int32_t extraFlags(In_ImGML_IntOpt(0));
        Out_ImGML_Bool(ImGui::InputInt(label.c_str(), &v, step, stepFast, extraFlags));
        Out_ImGML_Int(v);
        return 1.0;
    }

    ////
    //IMGUI_API bool          InputInt2(const char* label, int v[2], ImGuiInputTextFlags extra_flags = 0);
    IMGML InputInt2() {
        const uint8_t Elems(2);
        int32_t v[Elems];
        CheckImGML_InOut();
        auto label(In_ImGML_String());
        for (uint8_t i(0); i < Elems; ++i) v[i] = In_ImGML_Int();
        int32_t extraFlags(In_ImGML_IntOpt(0));
        Out_ImGML_Bool(ImGui::InputInt2(label.c_str(), v, extraFlags));
        for (uint8_t i(0); i < Elems; ++i) Out_ImGML_Int(v[i]);
        return 1.0;
    }

    ////
    //IMGUI_API bool          InputInt3(const char* label, int v[3], ImGuiInputTextFlags extra_flags = 0);
    IMGML InputInt3() {
        const uint8_t Elems(3);
        int32_t v[Elems];
        CheckImGML_InOut();
        auto label(In_ImGML_String());
        for (uint8_t i(0); i < Elems; ++i) v[i] = In_ImGML_Int();
        int32_t extraFlags(In_ImGML_IntOpt(0));
        Out_ImGML_Bool(ImGui::InputInt3(label.c_str(), v, extraFlags));
        for (uint8_t i(0); i < Elems; ++i) Out_ImGML_Int(v[i]);
        return 1.0;
    }

    ////
    //IMGUI_API bool          InputInt4(const char* label, int v[4], ImGuiInputTextFlags extra_flags = 0);
    IMGML InputInt4() {
        const uint8_t Elems(4);
        int32_t v[Elems];
        CheckImGML_InOut();
        auto label(In_ImGML_String());
        for (uint8_t i(0); i < Elems; ++i) v[i] = In_ImGML_Int();
        int32_t extraFlags(In_ImGML_IntOpt(0));
        Out_ImGML_Bool(ImGui::InputInt4(label.c_str(), v, extraFlags));
        for (uint8_t i(0); i < Elems; ++i) Out_ImGML_Int(v[i]);
        return 1.0;
    }
}
