#include "imguigml_wrappers.h"

extern "C" {

    ////////////
    // Menus
    ////////////

    ////
    // IMGUI_API bool          BeginMainMenuBar();
    //  create and append to a full screen menu-bar. only call EndMainMenuBar() if this returns true!
    IMGML BeginMainMenuBar() {
        CheckImGML_Out();
        Out_ImGML_Bool(ImGui::BeginMainMenuBar());
        return 1.0;
    }

    ////
    // IMGUI_API void          EndMainMenuBar();
    IMGML EndMainMenuBar() {
        CheckImGML();
        ImGui::EndMainMenuBar();
        return 1.0;
    }

    ////
    // IMGUI_API bool          BeginMenuBar();
    //  append to menu-bar of current window (requires ImGuiWindowFlags_MenuBar flag set on parent window). only call EndMenuBar() if this returns true!
    IMGML BeginMenuBar() {
        CheckImGML_Out();
        Out_ImGML_Bool(ImGui::BeginMenuBar());
        return 1.0;
    }

    ////
    //IMGUI_API void          EndMenuBar();
    IMGML EndMenuBar() {
        CheckImGML();
        ImGui::EndMenuBar();
        return 1.0;
    }
    ////
    // IMGUI_API bool          BeginMenu(const char* label, bool enabled = true);
    //  create a sub-menu entry. only call EndMenu() if this returns true!
    IMGML ImBeginMenu() {
        CheckImGML_InOut();
        std::string label(In_ImGML_String());
        bool enabled(In_ImGML_BoolOpt(true));
        Out_ImGML_Bool(ImGui::BeginMenu(label.c_str(), enabled));
        return 1.0;
    }

    ////
    // IMGUI_API void          EndMenu();
    IMGML ImEndMenu() {
        CheckImGML();
        try {
            ImGui::EndMenu();
        }
        catch (std::exception&) {
            int x = 0;
            int y = 1;
            x = y + x;
            y++;
        }
        return 1.0;
    }

    ////
    // IMGUI_API bool          MenuItem(const char* label, const char* shortcut, bool selected, bool enabled = true);
    //  return true when activated. 
    //  note: shortcuts are displayed for convenience but not processed by ImGui at the moment
    IMGML MenuItem() {
        CheckImGML_InOut();
        
        auto label(In_ImGML_String());
        auto shortcut(In_ImGML_String());
        auto selected(In_ImGML_Bool());
        auto enabled(In_ImGML_BoolOpt(true));
        Out_ImGML_Bool(ImGui::MenuItem(label.c_str(), shortcut.c_str(), selected, enabled));
        
        return 1.0;
    }

    ////
    // IMGUI_API void          OpenPopup(const char* str_id);
    //  call to mark popup as open (don't call every frame!). popups are closed when user click outside, or if CloseCurrentPopup() is called within a BeginPopup()/EndPopup() block. By default, Selectable()/MenuItem() are calling CloseCurrentPopup(). Popup identifiers are relative to the current ID-stack (so OpenPopup and BeginPopup needs to be at the same level).
    IMGML OpenPopup() {
        CheckImGML_In();
        ImGui::OpenPopup(In_ImGML_String().c_str());
        return 1.0;
    }

    ////
    // IMGUI_API bool          OpenPopupOnItemClick(const char* str_id = NULL, int mouse_button = 1);
    //  helper to open popup when clicked on last item. return true when just opened.
    IMGML OpenPopupOnItemClick() {
        CheckImGML_In();
        const char* strId(nullptr);
        std::string str("");
        int mouseButton(1);
        if (in.Count() > 0 && in.IsType<std::string>(0)) {
            str = In_ImGML_String();
            strId = str.c_str();
        }
        if (in.Count() > 1)
            mouseButton = in.Val<int>(1);
            
        ImGui::OpenPopupOnItemClick(strId, mouseButton);
        return 1.0;
    }

    ////
    // IMGUI_API bool          BeginPopup(const char* str_id);
    //  return true if the popup is open, and you can start outputting to it. only call EndPopup() if BeginPopup() returned true!
    IMGML BeginPopup() {
        CheckImGML_InOut();
        Out_ImGML_Bool(ImGui::BeginPopup(In_ImGML_String().c_str()));
        return 1.0;
    }

    ////
    // IMGUI_API bool          BeginPopupModal(const char* name, bool* p_open = NULL, ImGuiWindowFlags extra_flags = 0);
    //  modal dialog (block interactions behind the modal window, can't close the modal window by clicking outside)
    IMGML BeginPopupModal() {
        CheckImGML_InOut();
        bool* popen(nullptr);
        bool open(false);
        std::string name(In_ImGML_String());
        
        if (in.IsType<int8_t>(1)) {
            popen = &open;
            open = In_ImGML_Bool();
        } else 
            In_ImGML_String();
       
        ImGuiWindowFlags extraFlags(In_ImGML_IntOpt(0));

        bool ret(ImGui::BeginPopupModal(name.c_str(), popen, extraFlags));
        Out_ImGML_Bool(ret);
        if (popen != nullptr)
            Out_ImGML_Bool(*popen);
        return 1.0;
    }

    ////
    // IMGUI_API bool          BeginPopupContextItem(const char* str_id = NULL, int mouse_button = 1);
    //  helper to open and begin popup when clicked on last item. if you can pass a NULL str_id only if the previous item had an id. If you want to use that on a non-interactive item such as Text() you need to pass in an explicit ID here. read comments in .cpp!
    IMGML BeginPopupContextItem() {
        CheckImGML_InOut();
        const char* strId(nullptr);
        std::string str("");
        int mouseButton(1);
        if (in.Count() > 0 && in.IsType<std::string>(0)) {
            str = In_ImGML_String();
            strId = str.c_str();
        }
        if (in.Count() > 1)
            mouseButton = in.Val<int>(1);

        Out_ImGML_Bool(ImGui::BeginPopupContextItem(strId, mouseButton));
        return 1.0;
    }

    ////
    // IMGUI_API bool          BeginPopupContextWindow(const char* str_id = NULL, int mouse_button = 1, bool also_over_items = true);
    // helper to open and begin popup when clicked on current window.
    IMGML BeginPopupContextWindow() {
        CheckImGML_InOut();
        const char* strId(nullptr);
        std::string str("");
        int mouseButton(1);
        bool alsoOverItems(true);
        if (in.Count() > 0 && in.IsType<std::string>(0)) {
            str = In_ImGML_String();
            strId = str.c_str();
        }
        if (in.Count() > 1)
            mouseButton = in.Val<int>(1);
        if (in.Count() > 2)
            alsoOverItems = in.Val<int8_t>(2) != 0;

        Out_ImGML_Bool(ImGui::BeginPopupContextWindow(strId, mouseButton, alsoOverItems));
        return 1.0;
    }

    ////
    // IMGUI_API bool          BeginPopupContextVoid(const char* str_id = NULL, int mouse_button = 1);
    // helper to open and begin popup when clicked in void (where there are no imgui windows).
    IMGML BeginPopupContextVoid() {
        CheckImGML_InOut();
        const char* strId(nullptr);
        std::string str("");
        int mouseButton(1);
        if (in.Count() > 0 && in.IsType<std::string>(0)) {
            str = In_ImGML_String();
            strId = str.c_str();
        }
        if (in.Count() > 1)
            mouseButton = in.Val<int>(1);

        Out_ImGML_Bool(ImGui::BeginPopupContextVoid(strId, mouseButton));
        return 1.0;
    }

    ////
    // IMGUI_API void          EndPopup();
    IMGML EndPopup() {
        CheckImGML();
        ImGui::EndPopup();
        return 1.0;
    }

    ////
    // IMGUI_API bool          IsPopupOpen(const char* str_id);
    //  return true if the popup is open
    IMGML IsPopupOpen() {
        CheckImGML_InOut();
        Out_ImGML_Bool(ImGui::IsPopupOpen(In_ImGML_String().c_str()));
        return 1.0;
    }

    ////
    // IMGUI_API void          CloseCurrentPopup();
    //  close the popup we have begin-ed into. clicking on a MenuItem or Selectable automatically close the current popup.
    IMGML CloseCurrentPopup() {
        CheckImGML();
        ImGui::CloseCurrentPopup();
        return 1.0;
    }

}
