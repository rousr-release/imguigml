#include "imguigml_wrappers.h"

// IO { 
//  ImFontAtlas*  Fonts;                    // <auto>               // Load and assemble one or more fonts into a single tightly packed texture. Output to Fonts array.
//  ImFont*       FontDefault;              // = NULL               // Font to use on NewFrame(). Use NULL to uses Fonts->Fonts[0].
// }

namespace {
	std::vector<ImFont*> Fonts;
	std::stack<size_t> FreeIndices;
}

const char *environment_get_variable(const char *name) {
#if defined(_WIN32)
	static std::string value;
	DWORD length = 0;
	std::wstring u8name = widen(name);
	if ((length = GetEnvironmentVariableW(u8name.c_str(), nullptr, 0)) != 0) {
		wchar_t *buffer = new wchar_t[length]();
		if (GetEnvironmentVariableW(u8name.c_str(), buffer, length) != 0) {
			value = narrow(buffer);
		}
		delete[] buffer;
	}
	return value.c_str();
#else
	static std::string value;
	char *ptr = getenv(name);
	value = ((ptr) ? ptr : "");
	return value.c_str();
#endif
}

double environment_set_variable(const char *name, const char *value) {
#if defined(_WIN32)
	std::wstring u8name = widen(name);
	std::wstring u8value = widen(value);
	return (SetEnvironmentVariableW(u8name.c_str(), u8value.c_str()) != 0);
#else
	return (setenv(name, value, 1) == 0);
#endif
}

std::string fontFiles;
const char *imguigml_get_fontfiles() { return fontFiles.c_str(); }
double imguigml_set_fontfiles(const char *fontfiles) { fontFiles = fontfiles; return 1.0; }

double fontSize;
double imguigml_get_fontsize() { return fontSize; }
double imguigml_set_fontsize(double fontsize) { fontSize = fontsize; return 1.0; }

extern "C" {
	IMGML AddFontFromTTF(char* _data) {
		CheckImGML_InOut();
		
		const size_t _dataSize = static_cast<size_t>(In_ImGML_Int());
		const float _fontSize = In_ImGML_Float();
		const size_t _ttfIndex = static_cast<size_t>(In_ImGML_Int());
		const std::string _fontName = In_ImGML_String();

		void *ttfData = IM_ALLOC(_dataSize);
		std::memcpy(ttfData, _data, _dataSize);
		
		ImFontConfig font_cfg;
		std::snprintf(font_cfg.Name, 40, "%s, %.0fpx", _fontName.c_str(), _fontSize);

		auto font(io.Fonts->AddFontFromMemoryTTF(ttfData, static_cast<int>(_dataSize), _fontSize, &font_cfg));
 		if (_ttfIndex < Fonts.size())
 			Fonts[_ttfIndex] = font;
 
 		Out_ImGML_Bool(font != nullptr);

		return 1.0;
	}

	IMGML ReserveFontIndex() {
		CheckImGML_Out()
		size_t freeIndex(Fonts.size());
		if (!FreeIndices.empty())
		{
			freeIndex = FreeIndices.top();
			FreeIndices.pop();
			Fonts[freeIndex] = nullptr;
		}
		else {
			Fonts.push_back(nullptr);
		}
		Out_ImGML_Double(static_cast<double>(freeIndex));

		return 1.0;
	}

	IMGML IsFontValid() {
		CheckImGML_InOut();
		size_t index(static_cast<size_t>(In_ImGML_Double()));
		Out_ImGML_Bool(index < Fonts.size() && Fonts[index] != nullptr);
		return 1.0;
	}
	
	////
	// IMGUI_API void          PushFont(ImFont* font);
	//  use NULL as a shortcut to push default font
	IMGML PushFont() {
		CheckImGML_In();
		size_t fontIndex(static_cast<size_t>(In_ImGML_Double()));
		ImFont* font(fontIndex < Fonts.size() ? Fonts[fontIndex] : nullptr);
		ImGui::PushFont(font);
		return 1.0;
	}

	////
	// IMGUI_API void          PopFont();
	IMGML PopFont() {
		CheckImGML();
		ImGui::PopFont();
		return 1.0;
	}

	////
	// IMGUI_API ImFont*       GetFont();
	//  get current font
	IMGML GetFont() {
		CheckImGML_Out();
		ImFont* pFont(ImGui::GetFont());

		auto fontIter(std::find(std::begin(Fonts), std::end(Fonts), pFont));
		if (fontIter == std::end(Fonts)) {
			Out_ImGML_Double(-1.0);
		} else {
			Out_ImGML_Double(static_cast<double>(fontIter - std::begin(Fonts)));
		}

		return 1.0;
	}

	////
	// IMGUI_API float         GetFontSize();
	//  get current font size (= height in pixels) of current font with current scale applied
	IMGML GetFontSize() {
		CheckImGML_Out();
		Out_ImGML_Float(ImGui::GetFontSize());
		return 1.0;
	}

	////
	// IMGUI_API ImVec2        GetFontTexUvWhitePixel();
	//  get UV coordinate for a while pixel, useful to draw custom shapes via the ImDrawList API
	IMGML GetFontTexUvWhitePixel() {
		CheckImGML_Out();
		ImVec2 uv(ImGui::GetFontTexUvWhitePixel());
		Out_ImGML_Float(uv.x);
		Out_ImGML_Float(uv.y);
		return 1.0;
	}

}
