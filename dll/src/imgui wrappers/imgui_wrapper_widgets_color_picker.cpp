#include "imguigml_wrappers.h"

extern "C" {

    ///////////////
    // Widgets: Color Editor/Picker (tip: the ColorEdit* functions have a little colored preview square that can be left-clicked to open a picker, and right-clicked to open an option menu.)
    // Note that a 'float v[X]' function argument is the same as 'float* v', the array syntax is just a way to document the number of elements that are expected to be accessible. You can the pass the address of a first float element out of a contiguous structure, e.g. &myvector.x
    ///////////////

    ////
    // IMGUI_API bool          ColorEdit3(const char* label, float col[3], ImGuiColorEditFlags flags = 0);
    IMGML ColorEdit3() {
        CheckImGML_InOut();
        auto label(In_ImGML_String());
        float col[3] = { 0.0f, 0.0f, 0.0f };
        col[0] = In_ImGML_Float();
        col[1] = In_ImGML_Float();
        col[2] = In_ImGML_Float();
        int32_t flags(In_ImGML_IntOpt(0));
        bool ret(ImGui::ColorEdit3(label.c_str(), col, flags));
        Out_ImGML_Bool(ret);
        Out_ImGML_Float(col[0]);
        Out_ImGML_Float(col[1]);
        Out_ImGML_Float(col[2]);
        return 1.0;
    }

    ////
    // IMGUI_API bool          ColorEdit4(const char* label, float col[4], ImGuiColorEditFlags flags = 0);
    IMGML ColorEdit4() {
        CheckImGML_InOut();
        auto label(In_ImGML_String());
        float col[4] = { 0.0f, 0.0f, 0.0f, 0.0f };
        col[0] = In_ImGML_Float();
        col[1] = In_ImGML_Float();
        col[2] = In_ImGML_Float();
        col[3] = In_ImGML_Float();
        int32_t flags(In_ImGML_IntOpt(0));
        bool ret(ImGui::ColorEdit4(label.c_str(), col, flags));
        Out_ImGML_Bool(ret);
        Out_ImGML_Float(col[0]);
        Out_ImGML_Float(col[1]);
        Out_ImGML_Float(col[2]);
        Out_ImGML_Float(col[3]);
        return 1.0;
    }

    ////
    // IMGUI_API bool          ColorPicker3(const char* label, float col[3], ImGuiColorEditFlags flags = 0);
    IMGML ColorPicker3() {
        CheckImGML_InOut();
        auto label(In_ImGML_String());
        float col[3] = { 0.0f, 0.0f, 0.0f };
        col[0] = In_ImGML_Float();
        col[1] = In_ImGML_Float();
        col[2] = In_ImGML_Float();
        int32_t flags(In_ImGML_IntOpt(0));
        bool ret(ImGui::ColorPicker3(label.c_str(), col, flags));
        Out_ImGML_Bool(ret);
        Out_ImGML_Float(col[0]);
        Out_ImGML_Float(col[1]);
        Out_ImGML_Float(col[2]);
        return 1.0;
    }

    ////
    // IMGUI_API bool          ColorPicker4(const char* label, float col[4], ImGuiColorEditFlags flags = 0, const float* ref_col = NULL);
    IMGML ColorPicker4() {
        CheckImGML_InOut();
        auto label(In_ImGML_String());
        float col[4] = { 0.0f, 0.0f, 0.0f, 0.0f };
        //float ref[4];
        float *ref(nullptr);
        col[0] = In_ImGML_Float();
        col[1] = In_ImGML_Float();
        col[2] = In_ImGML_Float();
        col[3] = In_ImGML_Float();
        int32_t flags(In_ImGML_IntOpt(0));
        bool ret(ImGui::ColorPicker4(label.c_str(), col, flags, ref));
        Out_ImGML_Bool(ret);
        Out_ImGML_Float(col[0]);
        Out_ImGML_Float(col[1]);
        Out_ImGML_Float(col[2]);
        Out_ImGML_Float(col[3]);
        return 1.0;
    }

    ////
    // IMGUI_API bool          ColorButton(const char* desc_id, const ImVec4& col, ImGuiColorEditFlags flags = 0, ImVec2 size = ImVec2(0, 0));
    //  display a colored square/button, hover for details, return true when pressed.
    IMGML ColorButton() {
        CheckImGML_InOut();
        auto descId(In_ImGML_String());
        auto r(In_ImGML_Float());
        auto g(In_ImGML_Float());
        auto b(In_ImGML_Float());
        auto a(In_ImGML_Float());
        auto flags(In_ImGML_IntOpt(0));
        auto x(In_ImGML_FloatOpt(0.0f));
        auto y(In_ImGML_FloatOpt(0.0f));
        bool ret(ImGui::ColorButton(descId.c_str(), ImVec4(r, g, b, a), flags, ImVec2(x, y)));
        Out_ImGML_Bool(ret);
        return 1.0;
    }

    ////
    // IMGUI_API void          SetColorEditOptions(ImGuiColorEditFlags flags);
    //  initialize current options (generally on application startup) if you want to select a default format, picker type, etc. User will be able to change many settings, unless you pass the _NoOptions flag to your calls.
    IMGML SetColorEditOptions() {
        CheckImGML_In();
        ImGui::SetColorEditOptions(In_ImGML_Int());
        return 1.0;
    }

}