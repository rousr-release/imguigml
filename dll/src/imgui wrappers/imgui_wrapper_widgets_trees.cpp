#include "imguigml_wrappers.h"

extern "C" {
    /////
    // Widgets: Trees
    /////
    
    ////
    // IMGUI_API bool          TreeNode(const char* label);
    // IMGUI_API bool          TreeNode(const char* str_id, const char* fmt, ...) IM_FMTARGS(2);       
    // IMGUI_API bool          TreeNode(const void* ptr_id, const char* fmt, ...) IM_FMTARGS(2);       
    // IMGUI_API bool          TreeNodeV(const char* str_id, const char* fmt, va_list args) IM_FMTLIST(2);
    // IMGUI_API bool          TreeNodeV(const void* ptr_id, const char* fmt, va_list args) IM_FMTLIST(2);
    //  if returning 'true' the node is open and the tree id is pushed into the id stack. user is responsible for calling TreePop().
    //  // read the FAQ about why and how to use ID. to align arbitrary text at the same level as a TreeNode() you can use Bullet().
    IMGML TreeNode() {
        CheckImGML_InOut();
        bool ret(false);
        if (in.IsType<int32_t>(0)) {
            int32_t     ptr(In_ImGML_Int());
            std::string fmt(In_ImGML_String());
            ret = ImGui::TreeNode(reinterpret_cast<void*>(static_cast<size_t>(ptr)), "%s", fmt.c_str());
        } else {
            auto label(In_ImGML_String());
            if (in.Count() == 1)
                ret = ImGui::TreeNode(label.c_str());
            else
                ret = ImGui::TreeNode(label.c_str(), "%s", In_ImGML_String().c_str());
        }
        Out_ImGML_Bool(ret);
        return 1.0;
    }
    
    ////
    // IMGUI_API bool          TreeNodeEx(const char* label, ImGuiTreeNodeFlags flags = 0);
    // IMGUI_API bool          TreeNodeEx(const char* str_id, ImGuiTreeNodeFlags flags, const char* fmt, ...) IM_FMTARGS(3);
    // IMGUI_API bool          TreeNodeEx(const void* ptr_id, ImGuiTreeNodeFlags flags, const char* fmt, ...) IM_FMTARGS(3);
    // IMGUI_API bool          TreeNodeExV(const char* str_id, ImGuiTreeNodeFlags flags, const char* fmt, va_list args) IM_FMTLIST(3);
    // IMGUI_API bool          TreeNodeExV(const void* ptr_id, ImGuiTreeNodeFlags flags, const char* fmt, va_list args) IM_FMTLIST(3);
    IMGML TreeNodeEx() {
        CheckImGML_InOut();
        bool ret(false);
        if (in.IsType<int32_t>(0)) {
            int32_t ptr(In_ImGML_Int());
            auto flags(In_ImGML_Int());
            std::string fmt(In_ImGML_String());
            ret = ImGui::TreeNodeEx(reinterpret_cast<void*>(static_cast<size_t>(ptr)), flags, "%s", fmt.c_str());
        }
        else {
            auto label(In_ImGML_String());
            auto flags(In_ImGML_IntOpt(0));
            if (in.Count() < 3)
                ret = ImGui::TreeNodeEx(label.c_str(), flags);
            else
                ret = ImGui::TreeNodeEx(label.c_str(), flags, "%s", In_ImGML_String().c_str());
        }
        Out_ImGML_Bool(ret);
        return 1.0;
    }

    ////
    // IMGUI_API void          TreePush(const char* str_id = NULL);
    // IMGUI_API void          TreePush(const void* ptr_id = NULL);
    // ~ Indent()+PushId(). Already called by TreeNode() when returning true, but you can call Push/Pop yourself for layout purpose
    IMGML TreePush() {
        CheckImGML_In();
        if (in.IsType<int32_t>(0)) {
            int32_t     ptr(In_ImGML_Int());
            ImGui::TreePush(reinterpret_cast<void*>(static_cast<size_t>(ptr)));
        } else {
            auto label(In_ImGML_String());
            ImGui::TreePush(label.c_str());
        }
        return 1.0;
    }

    ////
    // IMGUI_API void          TreePop();
    // ~ Unindent()+PopId()
    IMGML TreePop() {
        CheckImGML();
        ImGui::TreePop();
        return 1.0;
    }

    ////
    // IMGUI_API void          TreeAdvanceToLabelPos();
    //  advance cursor x position by GetTreeNodeToLabelSpacing()
    IMGML TreeAdvanceToLabelPos() {
        CheckImGML();
        TreeAdvanceToLabelPos();
        return 1.0;
    }

    ////
    // IMGUI_API float         GetTreeNodeToLabelSpacing();
    //  horizontal distance preceding label when using TreeNode*() or Bullet() == (g.FontSize + style.FramePadding.x*2) for a regular unframed TreeNode
    IMGML GetTreeNodeToLabelSpacing() {
        CheckImGML_Out();
        Out_ImGML_Float(ImGui::GetTreeNodeToLabelSpacing());
        return 1.0;
    }

    ////
    // IMGUI_API void          SetNextTreeNodeOpen(bool is_open, ImGuiCond cond = 0);               
    //  set next TreeNode/CollapsingHeader open state.
    IMGML SetNextTreeNodeOpen() {
        CheckImGML_In();
        bool isOpen(In_ImGML_Bool());
        ImGuiCond cond(static_cast<ImGuiCond>(In_ImGML_IntOpt(0)));
        ImGui::SetNextTreeNodeOpen(isOpen, cond);
        return 1.0;
    }

    ////
    // IMGUI_API bool          CollapsingHeader(const char* label, ImGuiTreeNodeFlags flags = 0);               
    //  if returning 'true' the header is open. doesn't indent nor push on ID stack. user doesn't have to call TreePop().
    // IMGUI_API bool          CollapsingHeader(const char* label, bool* p_open, ImGuiTreeNodeFlags flags = 0); 
    //   when 'p_open' isn't NULL, display an additional small close button on upper right of the header
    IMGML CollapsingHeader() {
        bool *popen(nullptr);
        bool open(false);
        CheckImGML_InOut();
        
        auto label(In_ImGML_String());
        if (in.NextType<int8_t>()) { open = In_ImGML_Bool(); popen = &open; } else In_ImGML_Skip();
        auto flags(In_ImGML_IntOpt(0));
        Out_ImGML_Bool(ImGui::CollapsingHeader(label.c_str(), popen, flags));
        if (popen != nullptr) Out_ImGML_Bool(open);
        return 1.0;
    }
}