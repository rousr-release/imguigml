#include "imguigml_wrappers.h"

namespace {
    static bool ImGML_WindowDrawlist(true);
}

//#define IMGML_DRAWLIST ImDrawList* drawList(ImGML_WindowDrawlist ? ImGui::GetWindowDrawList() : ImGui::GetOverlayDrawList()); if (drawList == nullptr) { return 1.0; }

extern "C" {
    
    ////
    // IMGUI_API void  PushClipRect(ImVec2 clip_rect_min, ImVec2 clip_rect_max, bool intersect_with_current_clip_rect = false);  // Render-level scissoring. This is passed down to your render function but not used for CPU-side coarse clipping. Prefer using higher-level ImGui::PushClipRect() to affect logic (hit-testing and widget culling)
    IMGML DrawPushClipRect() {
        CheckImGML_In();
        auto minX(In_ImGML_Float());
        auto minY(In_ImGML_Float());
        auto maxX(In_ImGML_Float());
        auto maxY(In_ImGML_Float());
        auto intersectWithCurrentClipRect(In_ImGML_BoolOpt(false));
            
        ImDrawList* drawList(ImGui::GetWindowDrawList());
        drawList->PushClipRect(ImVec2(minX, minY), ImVec2(maxX, maxY), intersectWithCurrentClipRect);

        return 1.0;
    }

    ////
    // IMGUI_API void  PushClipRectFullScreen();
    IMGML PushClipRectFullScreen() {
        CheckImGML();
        ImDrawList* drawList(ImGui::GetWindowDrawList());
        drawList->PushClipRectFullScreen();
        return 1.0;
    }

    ////
    // IMGUI_API void  PopClipRect();
    IMGML DrawPopClipRect() {
        CheckImGML();
        ImDrawList* drawList(ImGui::GetWindowDrawList());
        drawList->PopClipRect();
        return 1.0;
    }

    ////
    // IMGUI_API void  PushTextureID(const ImTextureID& texture_id);
    IMGML PushTextureID() {
        CheckImGML_In();
        auto texId(In_ImGML_Uint());

        ImDrawList* drawList(ImGui::GetWindowDrawList());
        drawList->PushTextureID(reinterpret_cast<void*>(static_cast<size_t>(texId)));

        return 1.0;
    }
    
    ////
    // IMGUI_API void  PopTextureID();
    IMGML PopTextureID() {
        CheckImGML();
        ImDrawList* drawList(ImGui::GetWindowDrawList());
        drawList->PopTextureID();
        return 1.0;
    }

    ////
    // inline ImVec2   GetClipRectMin() const
    IMGML GetClipRectMin() {
        CheckImGML_Out();
        ImDrawList* drawList(ImGui::GetWindowDrawList());
        auto minRect(drawList->GetClipRectMin());
        Out_ImGML_Float(minRect.x);
        Out_ImGML_Float(minRect.y);
        return 1.0;
    }

    ////
    // inline ImVec2   GetClipRectMax() const
    IMGML GetClipRectMax() {
        CheckImGML_Out();
        ImDrawList* drawList(ImGui::GetWindowDrawList());
        auto maxRect(drawList->GetClipRectMax());
        Out_ImGML_Float(maxRect.x);
        Out_ImGML_Float(maxRect.y);
        return 1.0;
    }

    ///////////////
    // Primitives
    ///////////////
    
    IMGML SetDrawlistFlags() {
        CheckImGML_In();
        auto flags(In_ImGML_Uint());
        ImDrawList* drawList(ImGui::GetWindowDrawList());
        if (drawList == nullptr)
            return -1.0;
        
        drawList->Flags = flags;
        return 1.0;
    }

    IMGML GetDrawListFlags() {
        CheckImGML_InOut();
        ImDrawList* drawList(ImGui::GetWindowDrawList());
        if (drawList == nullptr)
            return -1.0;
        
        Out_ImGML_Uint(drawList->Flags);
        return 1.0;
    }

    IMGML UseOverlayDrawlist() {
        ImGML_WindowDrawlist = false;
        return 1.0;
    }

    IMGML UseWindowDrawList() {
        ImGML_WindowDrawlist = true;
        return 1.0;
    }

    ////
    // IMGUI_API void  AddLine(const ImVec2& a, const ImVec2& b, ImU32 col, float thickness = 1.0f);
    IMGML AddLine() {
        CheckImGML_In();
        auto aX(In_ImGML_Float());
        auto aY(In_ImGML_Float());
        auto bX(In_ImGML_Float());
        auto bY(In_ImGML_Float());
        auto col(In_ImGML_Uint());
        auto thickness(In_ImGML_FloatOpt(1.0f));

        ImDrawList* drawList(ImGui::GetWindowDrawList());
        drawList->AddLine(ImVec2(aX, aY), ImVec2(bX, bY), col, thickness);

        return 1.0;
    }

    ////
    // IMGUI_API void  AddRect(const ImVec2& a, const ImVec2& b, ImU32 col, float rounding = 0.0f, int rounding_corners_flags = ~0, float thickness = 1.0f);   
    // a: upper-left, b: lower-right, rounding_corners_flags: 4-bits corresponding to which corner to round
    IMGML AddRect() {
        CheckImGML_In();
        auto aX(In_ImGML_Float());
        auto aY(In_ImGML_Float());
        auto bX(In_ImGML_Float());
        auto bY(In_ImGML_Float());
        auto col(In_ImGML_Uint());
        auto rounding(In_ImGML_FloatOpt(0.0f));
        auto roundingCornersFlags(In_ImGML_IntOpt(0));
        auto thickness(In_ImGML_FloatOpt(1.0f));

        ImDrawList* drawList(ImGui::GetWindowDrawList());
        drawList->AddRect(ImVec2(aX, aY), ImVec2(bX, bY), col, rounding, roundingCornersFlags, thickness);

        return 1.0;
    }

    ////
    // IMGUI_API void  AddRectFilled(const ImVec2& a, const ImVec2& b, ImU32 col, float rounding = 0.0f, int rounding_corners_flags = ~0);
    // a: upper-left, b: lower-right
    IMGML AddRectFilled() {
        CheckImGML_In();
        auto aX(In_ImGML_Float());
        auto aY(In_ImGML_Float());
        auto bX(In_ImGML_Float());
        auto bY(In_ImGML_Float());
        auto col(In_ImGML_Uint());
        auto rounding(In_ImGML_FloatOpt(0.0f));
        auto roundingCornersFlags(In_ImGML_IntOpt(0));
        
        ImDrawList* drawList(ImGui::GetWindowDrawList());
        drawList->AddRectFilled(ImVec2(aX, aY), ImVec2(bX, bY), col, rounding, roundingCornersFlags);

        return 1.0;
    }

    ////
    // IMGUI_API void  AddRectFilledMultiColor(const ImVec2& a, const ImVec2& b, ImU32 col_upr_left, ImU32 col_upr_right, ImU32 col_bot_right, ImU32 col_bot_left);
    IMGML AddRectFilledMultiColor() {
        CheckImGML_In();
        auto aX(In_ImGML_Float());
        auto aY(In_ImGML_Float());
        auto bX(In_ImGML_Float());
        auto bY(In_ImGML_Float());
        auto colUL(In_ImGML_Uint());
        auto colUR(In_ImGML_Uint());
        auto colBR(In_ImGML_Uint());
        auto colBL(In_ImGML_Uint());

        ImDrawList* drawList(ImGui::GetWindowDrawList());
        drawList->AddRectFilledMultiColor(ImVec2(aX, aY), ImVec2(bX, bY), colUL, colUR, colBR, colBL);

        return 1.0;
    }

    ////
    // IMGUI_API void  AddQuad(const ImVec2& a, const ImVec2& b, const ImVec2& c, const ImVec2& d, ImU32 col, float thickness = 1.0f);
    IMGML AddQuad() {
        CheckImGML_In();
        auto aX(In_ImGML_Float());
        auto aY(In_ImGML_Float());
        auto bX(In_ImGML_Float());
        auto bY(In_ImGML_Float());
        auto cX(In_ImGML_Float());
        auto cY(In_ImGML_Float());
        auto dX(In_ImGML_Float());
        auto dY(In_ImGML_Float());
        auto col(In_ImGML_Uint());
        auto thickness(In_ImGML_FloatOpt(1.0f));

        ImDrawList* drawList(ImGui::GetWindowDrawList());
        drawList->AddQuad(ImVec2(aX, aY), ImVec2(bX, bY), ImVec2(cX, cY), ImVec2(dX, dY), col, thickness);

        return 1.0;
    }

    ////
    // IMGUI_API void  AddQuadFilled(const ImVec2& a, const ImVec2& b, const ImVec2& c, const ImVec2& d, ImU32 col);
    IMGML AddQuadFilled() {
        CheckImGML_In();
        auto aX(In_ImGML_Float());
        auto aY(In_ImGML_Float());
        auto bX(In_ImGML_Float());
        auto bY(In_ImGML_Float());
        auto cX(In_ImGML_Float());
        auto cY(In_ImGML_Float());
        auto dX(In_ImGML_Float());
        auto dY(In_ImGML_Float());
        auto col(In_ImGML_Uint());
        
        ImDrawList* drawList(ImGui::GetWindowDrawList());
        drawList->AddQuadFilled(ImVec2(aX, aY), ImVec2(bX, bY), ImVec2(cX, cY), ImVec2(dX, dY), col);

        return 1.0;
    }

    ////
    // IMGUI_API void  AddTriangle(const ImVec2& a, const ImVec2& b, const ImVec2& c, ImU32 col, float thickness = 1.0f);
    IMGML AddTriangle() {
        CheckImGML_In();
        auto aX(In_ImGML_Float());
        auto aY(In_ImGML_Float());
        auto bX(In_ImGML_Float());
        auto bY(In_ImGML_Float());
        auto cX(In_ImGML_Float());
        auto cY(In_ImGML_Float());
        auto col(In_ImGML_Uint());
        auto thickness(In_ImGML_FloatOpt(1.0f));

        ImDrawList* drawList(ImGui::GetWindowDrawList());
        drawList->AddTriangle(ImVec2(aX, aY), ImVec2(bX, bY), ImVec2(cX, cY), col, thickness);

        return 1.0;
    }

    ////
    // IMGUI_API void  AddTriangleFilled(const ImVec2& a, const ImVec2& b, const ImVec2& c, ImU32 col);
    IMGML AddTriangleFilled() {
        CheckImGML_In();
        auto aX(In_ImGML_Float());
        auto aY(In_ImGML_Float());
        auto bX(In_ImGML_Float());
        auto bY(In_ImGML_Float());
        auto cX(In_ImGML_Float());
        auto cY(In_ImGML_Float());
        auto col(In_ImGML_Uint());
        
        ImDrawList* drawList(ImGui::GetWindowDrawList());
        drawList->AddTriangleFilled(ImVec2(aX, aY), ImVec2(bX, bY), ImVec2(cX, cY), col);

        return 1.0;
    }

    ////
    // IMGUI_API void  AddCircle(const ImVec2& centre, float radius, ImU32 col, int num_segments = 12, float thickness = 1.0f);
    IMGML AddCircle() {
        CheckImGML_In();
        auto centreX(In_ImGML_Float());
        auto centreY(In_ImGML_Float());
        auto radius(In_ImGML_Float());
        auto col(In_ImGML_Uint());
        auto numSegments(In_ImGML_IntOpt(12));
        auto thickness(In_ImGML_FloatOpt(1.0f));

        ImDrawList* drawList(ImGui::GetWindowDrawList());
        drawList->AddCircle(ImVec2(centreX, centreY), radius, col, numSegments, thickness);

        return 1.0;
    }

    ////
    // IMGUI_API void  AddCircleFilled(const ImVec2& centre, float radius, ImU32 col, int num_segments = 12);
    IMGML AddCircleFilled() {
        CheckImGML_In();
        auto centreX(In_ImGML_Float());
        auto centreY(In_ImGML_Float());
        auto radius(In_ImGML_Float());
        auto col(In_ImGML_Uint());
        auto numSegments(In_ImGML_IntOpt(12));

        ImDrawList* drawList(ImGui::GetWindowDrawList());
        drawList->AddCircleFilled(ImVec2(centreX, centreY), radius, col, numSegments);

        return 1.0;
    }

    ////
    // IMGUI_API void  AddText(const ImVec2& pos, ImU32 col, const char* text_begin, const char* text_end = NULL);
    // IMGUI_API void  AddText(const ImFont* font, float font_size, const ImVec2& pos, ImU32 col, const char* text_begin, const char* text_end = NULL, float wrap_width = 0.0f, const ImVec4* cpu_fine_clip_rect = NULL);
    IMGML AddText() {
        CheckImGML_In();
        auto posX(In_ImGML_Float());
        auto posY(In_ImGML_Float());
        auto col(In_ImGML_Uint());
        auto text(In_ImGML_String());
        auto wrapWidth(In_ImGML_Float());
  
        ImDrawList* drawList(ImGui::GetWindowDrawList());
        drawList->AddText(nullptr, 0.0f, ImVec2(posX, posY), col, text.c_str(), static_cast<const char*>(0), wrapWidth, nullptr);

        return 1.0;
    }

    ////
    // IMGUI_API void  AddImage(ImTextureID user_texture_id, const ImVec2& a, const ImVec2& b, const ImVec2& uv_a = ImVec2(0, 0), const ImVec2& uv_b = ImVec2(1, 1), ImU32 col = 0xFFFFFFFF);
    IMGML AddImage() {
        CheckImGML_In();
        auto texId(In_ImGML_Uint());
        auto aX(In_ImGML_Float());
        auto aY(In_ImGML_Float());
        auto bX(In_ImGML_Float());
        auto bY(In_ImGML_Float());
        auto uv0_X(In_ImGML_FloatOpt(0.0f));
        auto uv0_Y(In_ImGML_FloatOpt(0.0f));
        auto uv1_X(In_ImGML_FloatOpt(1.0f));
        auto uv1_Y(In_ImGML_FloatOpt(1.0f));
        auto col(In_ImGML_UintOpt(0xFFFFFFFF));

        ImDrawList* drawList(ImGui::GetWindowDrawList());
        drawList->AddImage(reinterpret_cast<void*>(static_cast<size_t>(texId)), ImVec2(aX, aY), ImVec2(bX, bY), ImVec2(uv0_X, uv0_Y), ImVec2(uv1_X, uv1_Y), col);

        return 1.0;
    }

    ////
    // IMGUI_API void  AddImageQuad(ImTextureID user_texture_id, const ImVec2& a, const ImVec2& b, const ImVec2& c, const ImVec2& d, const ImVec2& uv_a = ImVec2(0, 0), const ImVec2& uv_b = ImVec2(1, 0), const ImVec2& uv_c = ImVec2(1, 1), const ImVec2& uv_d = ImVec2(0, 1), ImU32 col = 0xFFFFFFFF);
    IMGML AddImageQuad() {
        CheckImGML_In();
        auto texId(In_ImGML_Uint());
        auto aX(In_ImGML_Float());
        auto aY(In_ImGML_Float());

        auto bX(In_ImGML_Float());
        auto bY(In_ImGML_Float());
        
        auto cX(In_ImGML_Float());
        auto cY(In_ImGML_Float());
        
        auto dX(In_ImGML_Float());
        auto dY(In_ImGML_Float());
        
        auto uv0_X(In_ImGML_FloatOpt(0.0f)); //TL
        auto uv0_Y(In_ImGML_FloatOpt(0.0f));

        auto uv1_X(In_ImGML_FloatOpt(1.0f)); //TR
        auto uv1_Y(In_ImGML_FloatOpt(0.0f));
        
        auto uv2_X(In_ImGML_FloatOpt(1.0f)); //BR
        auto uv2_Y(In_ImGML_FloatOpt(1.0f));
        
        auto uv3_X(In_ImGML_FloatOpt(0.0f)); //BL
        auto uv3_Y(In_ImGML_FloatOpt(1.0f));
        auto col(In_ImGML_UintOpt(0xFFFFFFFF));

        ImDrawList* drawList(ImGui::GetWindowDrawList());
        drawList->AddImageQuad(reinterpret_cast<void*>(static_cast<size_t>(texId)), ImVec2(aX, aY), ImVec2(bX, bY), ImVec2(cX, cY), ImVec2(dX, dY), ImVec2(uv0_X, uv0_Y), ImVec2(uv1_X, uv1_Y), ImVec2(uv2_X, uv2_Y), ImVec2(uv3_X, uv3_Y), col);

        return 1.0;
    }

    ////
    // IMGUI_API void  AddPolyline(const ImVec2* points, const int num_points, ImU32 col, bool closed, float thickness, bool anti_aliased);
    IMGML AddPolyline() {
        std::vector<ImVec2> points;
        CheckImGML_In();
        auto numPoints(In_ImGML_Int());
        for (int32_t i(0); i < numPoints; ++i) {
            auto x(In_ImGML_Float());
            auto y(In_ImGML_Float());
            points.push_back(ImVec2(x, y));
        }
        auto col(In_ImGML_Uint());
        auto closed(In_ImGML_Bool());
        auto thickness(In_ImGML_Float());

        ImDrawList* drawList(ImGui::GetWindowDrawList());
        drawList->AddPolyline(points.data(), numPoints, col, closed, thickness);

        return 1.0;
    }

    ////
    // IMGUI_API void  AddConvexPolyFilled(const ImVec2* points, const int num_points, ImU32 col, bool anti_aliased);
    IMGML AddConvexPolyFilled() {
        std::vector<ImVec2> points;
        CheckImGML_In();
        auto numPoints(In_ImGML_Int());
        for (int32_t i(0); i < numPoints; ++i) {
            auto x(In_ImGML_Float());
            auto y(In_ImGML_Float());
            points.push_back(ImVec2(x, y));
        }
        auto col(In_ImGML_Uint());

        ImDrawList* drawList(ImGui::GetWindowDrawList());
        drawList->AddConvexPolyFilled(points.data(), numPoints, col);

        return 1.0;
    }

    ////
    // IMGUI_API void  AddBezierCurve(const ImVec2& pos0, const ImVec2& cp0, const ImVec2& cp1, const ImVec2& pos1, ImU32 col, float thickness, int num_segments = 0);
    IMGML AddBezierCurve() {
        CheckImGML_In();
        auto pos0_X(In_ImGML_Float());
        auto pos0_Y(In_ImGML_Float());
        auto cp0_X(In_ImGML_Float());
        auto cp0_Y(In_ImGML_Float());
        auto cp1_X(In_ImGML_Float());
        auto cp1_Y(In_ImGML_Float());
        auto pos1_X(In_ImGML_Float());
        auto pos1_Y(In_ImGML_Float());
        auto col(In_ImGML_Uint());
        auto thickness(In_ImGML_Float());
        auto numSegments(In_ImGML_IntOpt(0));

        ImDrawList* drawList(ImGui::GetWindowDrawList());
        drawList->AddBezierCurve(ImVec2(pos0_X, pos0_Y), ImVec2(cp0_X, cp0_Y), ImVec2(cp1_X, cp1_Y), ImVec2(pos1_X, pos1_Y), col, thickness, numSegments);

        return 1.0;
    }


    ////////////
    // Stateful path API, add points then finish with PathFill() or PathStroke()
    ////////////

    ////
    // inline    void  PathClear()
    IMGML PathClear() {
        CheckImGML();
        ImDrawList* drawList(ImGui::GetWindowDrawList());
        drawList->PathClear();
        return 1.0;
    }

    ////
    // inline    void  PathLineTo(const ImVec2& pos)
    IMGML PathLineTo() {
        CheckImGML_In();
        auto posX(In_ImGML_Float());
        auto posY(In_ImGML_Float());
        ImDrawList* drawList(ImGui::GetWindowDrawList());
        drawList->PathLineTo(ImVec2(posX, posY));
        return 1.0;
    }

    ////
    // inline    void  PathLineToMergeDuplicate(const ImVec2& pos) 
    IMGML PathLineToMergeDuplicate() {
        CheckImGML_In();
        auto posX(In_ImGML_Float());
        auto posY(In_ImGML_Float());
        ImDrawList* drawList(ImGui::GetWindowDrawList());
        drawList->PathLineToMergeDuplicate(ImVec2(posX, posY));
        return 1.0;
    }

    ////
    // inline    void  PathFillConvex(ImU32 col)
    IMGML PathFillConvex() {
        CheckImGML_In();
        auto col(In_ImGML_Uint());
        ImDrawList* drawList(ImGui::GetWindowDrawList());
        drawList->PathFillConvex(col);
        return 1.0;
    }

    ////
    // inline    void  PathStroke(ImU32 col, bool closed, float thickness = 1.0f) 
    IMGML PathStroke() {
        CheckImGML_In();
        auto col(In_ImGML_Uint());
        auto closed(In_ImGML_Bool());
        auto thickness(In_ImGML_FloatOpt(1.0f));

        ImDrawList* drawList(ImGui::GetWindowDrawList());
        drawList->PathStroke(col, closed, thickness);
        return 1.0;
    }

    ////
    // IMGUI_API void  PathArcTo(const ImVec2& centre, float radius, float a_min, float a_max, int num_segments = 10);
    IMGML PathArcTo() {
        CheckImGML_In();
        auto centreX(In_ImGML_Float());
        auto centreY(In_ImGML_Float());
        auto radius(In_ImGML_Float());
        auto aMin(In_ImGML_Float());
        auto aMax(In_ImGML_Float());
        auto numSegments(In_ImGML_IntOpt(10));

        ImDrawList* drawList(ImGui::GetWindowDrawList());
        drawList->PathArcTo(ImVec2(centreX, centreY), radius, aMin, aMax, numSegments);

        return 1.0;
    }

    ////
    // IMGUI_API void  PathArcToFast(const ImVec2& centre, float radius, int a_min_of_12, int a_max_of_12);
    // Use precomputed angles for a 12 steps circle
    IMGML PathArcToFast() {
        CheckImGML_In();
        auto centreX(In_ImGML_Float());
        auto centreY(In_ImGML_Float());
        auto radius(In_ImGML_Float());
        auto aMin(In_ImGML_Int());
        auto aMax(In_ImGML_Int());

        ImDrawList* drawList(ImGui::GetWindowDrawList());
        drawList->PathArcToFast(ImVec2(centreX, centreY), radius, aMin, aMax);

        return 1.0;
    }

    ////
    // IMGUI_API void  PathBezierCurveTo(const ImVec2& p1, const ImVec2& p2, const ImVec2& p3, int num_segments = 0);
    IMGML PathBezierCurveTo() {
        CheckImGML_In();
        auto p1X(In_ImGML_Float());
        auto p1Y(In_ImGML_Float());
        auto p2X(In_ImGML_Float());
        auto p2Y(In_ImGML_Float());
        auto p3X(In_ImGML_Float());
        auto p3Y(In_ImGML_Float());
        auto numSegments(In_ImGML_IntOpt(0));

        ImDrawList* drawList(ImGui::GetWindowDrawList());
        drawList->PathBezierCurveTo(ImVec2(p1X, p1Y), ImVec2(p2X, p2Y), ImVec2(p3X, p3Y), numSegments);

        return 1.0;
    }

    ////
    // IMGUI_API void  PathRect(const ImVec2& rect_min, const ImVec2& rect_max, float rounding = 0.0f, int rounding_corners_flags = 0);
    // rounding_corners_flags: 4-bits corresponding to which corner to round
    IMGML PathRect() {
        CheckImGML_In();
        auto minX(In_ImGML_Float());
        auto minY(In_ImGML_Float());
        auto maxX(In_ImGML_Float());
        auto maxY(In_ImGML_Float());
        auto rounding(In_ImGML_FloatOpt(0.0f));
        auto roundingCornerFlags(In_ImGML_IntOpt(0));

        ImDrawList* drawList(ImGui::GetWindowDrawList());
        drawList->PathRect(ImVec2(minX, minY), ImVec2(maxX, maxY), rounding, roundingCornerFlags);

        return 1.0;
    }

    ////
    // Channels
    // - Use to simulate layers. By switching channels to can render out-of-order (e.g. submit foreground primitives before background primitives)
    // - Use to minimize draw calls (e.g. if going back-and-forth between multiple non-overlapping clipping rectangles, prefer to append into separate channels then merge at the end)
    ////
    // IMGUI_API void  ChannelsSplit(int channels_count);
    IMGML ChannelsSplit() {
        CheckImGML_In();
        auto channelsCount(In_ImGML_Int());
        ImDrawList* drawList(ImGui::GetWindowDrawList());
        drawList->ChannelsSplit(channelsCount);
        return 1.0;
    }
    
    ////
    // IMGUI_API void  ChannelsMerge();
    IMGML ChannelsMerge() {
        CheckImGML();
        ImDrawList* drawList(ImGui::GetWindowDrawList());
        drawList->ChannelsMerge();
        return 1.0;
    }
    ////
    // IMGUI_API void  ChannelsSetCurrent(int channel_index);
    IMGML ChannelsSetCurrent() {
        CheckImGML_In();
        auto channelIndex(In_ImGML_Int());
        ImDrawList* drawList(ImGui::GetWindowDrawList());
        drawList->ChannelsSetCurrent(channelIndex);
        return 1.0;
    }

    //// Unsupported
    // Advanced
    //IMGUI_API void  AddCallback(ImDrawCallback callback, void* callback_data);  // Your rendering function must check for 'UserCallback' in ImDrawCmd and call the function instead of rendering triangles.
    //IMGUI_API void  AddDrawCmd();                                               // This is useful if you need to forcefully create a new draw call (to allow for dependent rendering / blending). Otherwise primitives are merged into the same draw-call as much as possible        
}
