#include "imguigml_wrappers.h"

extern "C" {
    ////
    // IMGUI_API bool          BeginCombo(const char* label, const char* preview_value, ImGuiComboFlags flags = 0);
    IMGML BeginCombo() {
        CheckImGML_InOut();
        auto label(In_ImGML_String());
        auto prev(In_ImGML_String());
        auto flags(In_ImGML_UintOpt(0));
        Out_ImGML_Bool(ImGui::BeginCombo(label.c_str(), prev.c_str(), flags));
        return 1.0;
    }

    ////
    // IMGUI_API void           SetItemDefaultFocus();
    IMGML SetItemDefaultFocus() {
        CheckImGML();
        ImGui::SetItemDefaultFocus();
        return 1.0;
    }

    ////
    // IMGUI_API void          EndCombo();
    IMGML EndCombo() {
        CheckImGML();
        ImGui::EndCombo();
        return 1.0;
    }
    
    ////
    // IMGUI_API bool          Combo(const char* label, int* current_item, const char* const* items, int items_count, int height_in_items = -1);
    // Unsupported formats:
//   IMGUI_API bool          Combo(const char* label, int* current_item, const char* items_separated_by_zeros, int popup_max_height_in_items = -1);      // Separate items with \0 within a string, end item-list with \0\0. e.g. "One\0Two\0Three\0"
//   IMGUI_API bool          Combo(const char* label, int* current_item, bool(*items_getter)(void* data, int idx, const char** out_text), void* data, int items_count, int popup_max_height_in_items = -1);
    IMGML Combo() {
        std::vector<std::string> items;
        std::vector<const char*> itemNames;
        CheckImGML_InOut();
        auto label(In_ImGML_String());
        auto item(In_ImGML_Int());
        auto itemCount(In_ImGML_Int());
        for (int i(0); i < itemCount; ++i) items.push_back(In_ImGML_String());
        for (const auto& name : items) itemNames.push_back(name.c_str());
        auto heightInItems(In_ImGML_IntOpt(-1));
        Out_ImGML_Bool(ImGui::Combo(label.c_str(), &item, itemNames.data(), itemCount, heightInItems));
        Out_ImGML_Int(item);
        return 1.0;
    }

}